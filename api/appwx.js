// database.js

import cloudbase from '@cloudbase/js-sdk';
import registerWithEmail from '@/api/wxlogin';

registerWithEmail();

const app = cloudbase.init({
    env: "jvyingzhongbao-1g4qvecl6e75139d"
});

// 获取数据库实例
const db = app.database();

/**
 * 查询集合数据
 * @param {string} collection - 集合名称
 * @param {object} where - 查询条件，例如 {name: "张三"}
 * @param {object} options - 可选参数，如排序、限制数量等
 * @returns {Promise} 返回查询结果
 */
const query = async (collection, where = {}, options = {}) => {
  try {
    let query = db.collection(collection).where(where);
    
    // 处理排序
    if (options.orderBy) {
      query = query.orderBy(options.orderBy, options.order || 'asc');
    }
    
    // 处理数量限制
    if (options.limit) {
      query = query.limit(options.limit);
    }
    
    // 处理偏移
    if (options.offset) {
      query = query.skip(options.offset);
    }
    
    // 处理字段选择
    if (options.field) {
      query = query.field(options.field);
    }
    
    const res = await query.get();
    return res;
  } catch (error) {
    console.error('查询数据失败:', error);
    throw error;
  }
};

/**
 * 添加数据到集合
 * @param {string} collection - 集合名称
 * @param {object|array} data - 要添加的数据，可以是对象或对象数组
 * @returns {Promise} 返回添加结果
 */
const add = async (collection, data) => {
  try {
    if (Array.isArray(data)) {
      // 批量添加
      return await db.collection(collection).add(data);
    } else {
      // 单条添加
      return await db.collection(collection).add(data);
    }
  } catch (error) {
    console.error('添加数据失败:', error);
    throw error;
  }
};

/**
 * 更新集合中的数据
 * @param {string} collection - 集合名称
 * @param {object} where - 查询条件
 * @param {object} data - 要更新的数据
 * @returns {Promise} 返回更新结果
 */
const update = async (collection, where, data) => {
  try {
    return await db.collection(collection).where(where).update(data);
  } catch (error) {
    console.error('更新数据失败:', error);
    throw error;
  }
};

/**
 * 删除集合中的数据
 * @param {string} collection - 集合名称
 * @param {object} where - 查询条件
 * @returns {Promise} 返回删除结果
 */
const remove = async (collection, where) => {
  try {
    return await db.collection(collection).where(where).remove();
  } catch (error) {
    console.error('删除数据失败:', error);
    throw error;
  }
};

/**
 * 获取集合中的文档数量
 * @param {string} collection - 集合名称
 * @param {object} where - 查询条件
 * @returns {Promise} 返回计数结果
 */
const count = async (collection, where = {}) => {
  try {
    return await db.collection(collection).where(where).count();
  } catch (error) {
    console.error('获取数量失败:', error);
    throw error;
  }
};

/**
 * 获取文档详情
 * @param {string} collection - 集合名称
 * @param {string} id - 文档ID
 * @returns {Promise} 返回文档详情
 */
const getDocById = async (collection, id) => {
  try {
    return await db.collection(collection).doc(id).get();
  } catch (error) {
    console.error('获取文档详情失败:', error);
    throw error;
  }
};

// 导出数据库操作函数
export {
  db,
  query,
  add,
  update,
  remove,
  count,
  getDocById
};

export default app;

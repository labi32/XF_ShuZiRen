/**
 * 函数定义集合
 * 用于AI模型调用的函数定义
 * 
 * 重要提示：
 * 1. 所有函数定义都遵循OpenAI Function Calling格式
 * 2. 每个函数都有详细的参数说明和返回值说明
 * 3. 参数验证和错误处理在具体实现中完成
 * 4. 时间段定义：
 *    - 时间段1：8:00-10:00
 *    - 时间段2：10:00-12:00
 *    - 时间段3：14:00-16:00
 *    - 时间段4：16:00-18:00
 */

export const functionDefinitions = [
  {
    type: "function",
    function: {
      name: 'getTodayDate',
      description: '获取今日日期（格式：YYYY-MM-DD）',
      parameters: {
        type: 'object',
        properties: {}
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'queryLabs',
      description: '查询实验室列表（注意：这个函数查询的是实验室，不是设备）',
      parameters: {
        type: 'object',
        properties: {
          searchValue: {
            type: 'string',
            description: '搜索关键词，用于搜索实验室名称'
          },
          offset: {
            type: 'number',
            description: '分页起始位置'
          },
          pageSize: {
            type: 'number',
            description: '每页数量'
          }
        }
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'queryEquipment',
      description: '查询设备列表（注意：这个函数查询的是设备，不是实验室，sysId是可选参数）',
      parameters: {
        type: 'object',
        properties: {
          sysId: {
            type: 'string',
            description: '实验室ID（可选）。如果提供，则只返回该实验室下的设备；如果不提供，则返回所有设备'
          }
        }
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'queryReservations',
      description: '查询预约记录',
      parameters: {
        type: 'object',
        properties: {
          personId: {
            type: 'string',
            description: '用户ID，应使用缓存中user对象的_id字段'
          },
          sysName: {
            type: 'string',
            description: '实验室名称（用于搜索）'
          },
          offset: {
            type: 'number',
            description: '分页起始位置'
          },
          pageSize: {
            type: 'number',
            description: '每页数量'
          }
        },
        required: ['personId']
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'addReservation',
      description: '添加预约记录（注意：当前时间段内不能进行预约，且同一时间段不能被重复预约）',
      parameters: {
        type: 'object',
        properties: {
          personName: {
            type: 'string',
            description: '预约人姓名'
          },
          personId: {
            type: 'string',
            description: '预约人ID，应使用缓存中user对象的_id字段'
          },
          sysName: {
            type: 'string',
            description: '实验室名称'
          },
          sysId: {
            type: 'string',
            description: '实验室ID'
          },
          sbName: {
            type: 'string',
            description: '设备名称'
          },
          sbId: {
            type: 'string',
            description: '设备ID'
          },
          date: {
            type: 'string',
            description: '预约日期（格式：YYYY-MM-DD）'
          },
          timeSlots: {
            type: 'array',
            items: {
              type: 'number'
            },
            description: '预约时间段（必须是数组格式，如[1]表示8:00-10:00, [2]表示10:00-12:00, [3]表示14:00-16:00, [4]表示16:00-18:00）。注意：必须是数字数组格式，不要写成字符串"[3]"或["3"]这种格式。当前时间段内不能进行预约，且同一时间段不能被重复预约。'
          }
        },
        required: ['personName', 'personId', 'sysName', 'sysId', 'sbName', 'sbId', 'date', 'timeSlots']
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'getUserInfo',
      description: '获取当前登录用户的信息',
      parameters: {
        type: 'object',
        properties: {}
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'getStatistics',
      description: '获取实验室和设备统计信息',
      parameters: {
        type: 'object',
        properties: {}
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'reservationProcess',
      description: '预约流程函数，自动获取用户信息和当前日期，完成整个预约流程（注意：当前时间段内不能进行预约，且同一时间段不能被重复预约）',
      parameters: {
        type: 'object',
        properties: {
          sysName: {
            type: 'string',
            description: '实验室名称'
          },
          sysId: {
            type: 'string',
            description: '实验室ID'
          },
          sbName: {
            type: 'string',
            description: '设备名称'
          },
          sbId: {
            type: 'string',
            description: '设备ID'
          },
          date: {
            type: 'string',
            description: '预约日期（格式：YYYY-MM-DD），如不提供则使用当前日期'
          },
          timeSlots: {
            type: 'array',
            items: {
              type: 'number'
            },
            description: '预约时间段（必须是数组格式，如[1]表示8:00-10:00, [2]表示10:00-12:00, [3]表示14:00-16:00, [4]表示16:00-18:00）。注意：必须是数字数组格式，不要写成字符串"[3]"或["3"]这种格式。当前时间段内不能进行预约，且同一时间段不能被重复预约。'
          }
        },
        required: ['sysName', 'sysId', 'sbName', 'sbId', 'timeSlots']
      }
    }
  },
  {
    type: "function",
    function: {
      name: 'cancelReservation',
      description: '取消预约记录（注意：当前时间段内不能取消预约）',
      parameters: {
        type: 'object',
        properties: {
          reservationId: {
            type: 'string',
            description: '预约记录ID（支持直接传入ID字符串或对象参数，对象参数支持多种属性名：reservationId、id、_id、reservationreservationId）。注意：当前时间段内不能取消预约。'
          }
        },
        required: ['reservationId']
      }
    }
  }
]; 
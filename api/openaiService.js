// openaiService.js
// 封装与OpenAI API交互的服务

import OpenAI from "openai";
import functions from '@/api/function.js';
import { functionDefinitions } from '@/api/functionDefinitions.js';

// DEBUG开关，控制日志输出
const DEBUG = true;

// 添加全局日志函数
function debugLog(type, ...args) {
    if (!DEBUG) return;
    const timestamp = new Date().toISOString();
    switch (type) {
        case 'log':
            console.log(`[${timestamp}]`, ...args);
            break;
        case 'info':
            console.info(`[${timestamp}]`, ...args);
            break;
        case 'warn':
            console.warn(`[${timestamp}]`, ...args);
            break;
        case 'error':
            console.error(`[${timestamp}]`, ...args);
            break;
    }
}

// 创建不同配置的OpenAI实例
const openaiConfigs = {
    // 基础模型使用OpenAI配置
    'qwen-plus': new OpenAI({
        baseURL: 'https://dashscope.aliyuncs.com/compatible-mode/v1',
        apiKey: 'sk-b06fd3a49a724abbac6d4452aee90b37',
        dangerouslyAllowBrowser: true
    }),
    // 深度思考模型使用火山引擎配置
    'deepseek-r1-250120': new OpenAI({
        baseURL: 'https://ark.cn-beijing.volces.com/api/v3',
        apiKey: '09c7ad8f-1659-417b-b6e3-fc7b891061d4',
        dangerouslyAllowBrowser: true
    }),
    // 豆包模型使用OpenAI配置
    'doubao-1-5-pro-32k-250115': new OpenAI({
        baseURL: 'https://dashscope.aliyuncs.com/compatible-mode/v1',
        apiKey: 'sk-b06fd3a49a724abbac6d4452aee90b37',
        dangerouslyAllowBrowser: true
    })
};

// 获取当前模型对应的OpenAI实例
function getOpenAIInstance(modelName) {
    return openaiConfigs[modelName] || openaiConfigs['qwen-plus'];
}

/**
 * 解析函数参数
 * @param {String} functionName - 函数名称
 * @param {String} argsString - 参数字符串
 * @returns {Object} - 解析后的参数对象
 */
function parseFunctionArguments(functionName, argsString) {
    debugLog('info', '开始解析函数参数', { functionName, argsString });

    try {
        // 清理和预处理参数字符串
        let cleanedString = argsString
            .replace(/\s+/g, ' ')
            .replace(/,\s*}/g, '}')
            .replace(/,\s*\]/g, ']')
            .replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2":')
            .replace(/:\s*'([^']*)'/g, ':"$1"')
            .replace(/null$/, '')
            .trim();

        debugLog('log', '清理后的参数字符串', cleanedString);

        // 对特定参数类型进行检查和修复
        if (functionName === 'addReservation' || functionName === 'reservationProcess') {
            // 检查并修复timeSlots参数的格式
            try {
                const partialObj = JSON.parse(cleanedString);

                // 如果timeSlots参数存在但不是数组
                if (partialObj.timeSlots) {
                    if (typeof partialObj.timeSlots === 'string') {
                        // 尝试将字符串形式的数组转换为实际数组
                        if (partialObj.timeSlots.includes('[') && partialObj.timeSlots.includes(']')) {
                            // 处理 "[3]" 或 "['3']" 或 '["3"]' 形式的字符串
                            const matches = partialObj.timeSlots.match(/\[(['"]?)([1-4])(['"]?)\]/);
                            if (matches) {
                                partialObj.timeSlots = [Number(matches[2])];
                            }
                        } else if (/^[1-4]$/.test(partialObj.timeSlots)) {
                            // 处理单个数字或字符串的情况，如 "3"
                            partialObj.timeSlots = [Number(partialObj.timeSlots)];
                        }
                    } else if (Array.isArray(partialObj.timeSlots)) {
                        // 确保数组中的值是数字类型
                        partialObj.timeSlots = partialObj.timeSlots.map(slot => {
                            if (typeof slot === 'string') {
                                return Number(slot);
                            }
                            return slot;
                        });
                    }
                }
                // 修正后重新序列化
                cleanedString = JSON.stringify(partialObj);
            } catch (e) {
                debugLog('warn', '参数修复过程中出错，继续使用原始参数', e);
            }
        }

        // 尝试解析JSON
        const parsed = JSON.parse(cleanedString);
        debugLog('log', '成功解析JSON', parsed);
        return parsed;

    } catch (parseError) {
        debugLog('warn', 'JSON解析失败，尝试特定函数解析', parseError);
        return parseArgumentsManually(functionName, argsString);
    }
}

/**
 * 手动解析函数参数
 * @param {String} functionName - 函数名称
 * @param {String} argsString - 参数字符串
 * @returns {Object} - 解析后的参数对象
 */
function parseArgumentsManually(functionName, argsString) {
    console.error('修复后仍解析失败，使用简单解析');

    // 使用更简单的方法：直接构造对象
    let functionArgs = {};

    // 提取函数名称，确保我们知道需要哪些参数
    if (functionName === 'queryLabs') {
        return parseQueryLabsArguments(argsString);
    } else {
        return parseGenericArguments(argsString);
    }
}

/**
 * 解析queryLabs函数的参数
 * @param {String} argsString - 参数字符串
 * @returns {Object} - 解析后的参数对象
 */
function parseQueryLabsArguments(argsString) {
    debugLog('info', '开始解析queryLabs参数', { argsString });

    // 为queryLabs函数设置默认参数
    const functionArgs = {
        status: 0,
        searchValue: '',
        offset: 0,
        pageSize: 1000
    };

    try {
        // 尝试直接解析完整的JSON
        const parsed = JSON.parse(argsString);
        debugLog('log', '成功解析完整JSON', parsed);

        // 处理嵌套的offset对象
        if (parsed.offset && typeof parsed.offset === 'object') {
            functionArgs.offset = parsed.offset.offset || 0;
            functionArgs.pageSize = parsed.offset.pageSize || 1000;
        } else {
            // 合并解析出的值，保留默认值
            Object.assign(functionArgs, parsed);
        }

        debugLog('log', '最终解析结果', functionArgs);
        return functionArgs;

    } catch (error) {
        debugLog('warn', 'JSON解析失败，使用正则提取', error);

        try {
            // 清理字符串
            const cleanString = argsString
                .replace(/\\/g, '')
                .replace(/\s+/g, ' ')
                .trim();

            debugLog('log', '清理后的字符串', cleanString);

            // 提取status
            const statusMatch = cleanString.match(/status['":\s]+(-?\d+)/);
            if (statusMatch && statusMatch[1]) {
                functionArgs.status = Number(statusMatch[1]);
            }

            // 提取searchValue
            const searchMatch = cleanString.match(/searchValue['":\s]+"([^"]+)"/);
            if (searchMatch && searchMatch[1]) {
                functionArgs.searchValue = searchMatch[1];
            }

            // 提取offset
            const offsetMatch = cleanString.match(/offset['":\s]+(\{[^}]+\}|\d+)/);
            if (offsetMatch && offsetMatch[1]) {
                if (offsetMatch[1].startsWith('{')) {
                    // 处理嵌套的offset对象
                    try {
                        const offsetObj = JSON.parse(offsetMatch[1]);
                        functionArgs.offset = offsetObj.offset || 0;
                        functionArgs.pageSize = offsetObj.pageSize || 1000;
                    } catch (e) {
                        debugLog('warn', '嵌套offset解析失败，使用默认值', e);
                    }
                } else {
                    functionArgs.offset = Number(offsetMatch[1]);
                }
            }

            // 提取pageSize
            const pageSizeMatch = cleanString.match(/pageSize['":\s]+(\d+)/);
            if (pageSizeMatch && pageSizeMatch[1]) {
                functionArgs.pageSize = Number(pageSizeMatch[1]);
            }

        } catch (regexError) {
            debugLog('error', '正则提取失败，使用默认值', regexError);
        }
    }

    debugLog('info', '参数解析完成', functionArgs);
    return functionArgs;
}

/**
 * 解析通用函数参数
 * @param {String} argsString - 参数字符串
 * @returns {Object} - 解析后的参数对象
 */
function parseGenericArguments(argsString) {
    const functionArgs = {};
    const pairs = argsString
        .replace(/[{}]/g, '')
        .split(',');

    for (const pair of pairs) {
        if (pair.includes(':')) {
            const [key, value] = pair.split(':').map(s => s.trim());
            const cleanKey = key.replace(/['"]/g, '');
            let cleanValue = value.replace(/['"]/g, '');

            // 尝试将值转换为适当的类型
            if (cleanValue === 'true') cleanValue = true;
            else if (cleanValue === 'false') cleanValue = false;
            else if (cleanValue === 'null') cleanValue = null;
            else if (!isNaN(cleanValue) && cleanValue !== '') cleanValue = Number(cleanValue);

            functionArgs[cleanKey] = cleanValue;
        }
    }

    return functionArgs;
}

/**
 * 处理工具调用
 * @param {Array} deltaToolCalls - 工具调用增量数据
 * @param {Array} toolCalls - 当前工具调用集合
 * @param {Object} currentToolCall - 当前工具调用
 */
async function handleToolCalls(deltaToolCalls, toolCalls, currentToolCall) {
    debugLog('log', '处理工具调用数据:', deltaToolCalls);

    for (const toolCall of deltaToolCalls) {
        // 如果是新工具调用的开始
        if (toolCall.index !== undefined) {
            // 处理首次创建的工具调用
            if (!toolCalls[toolCall.index]) {
                currentToolCall = {
                    id: toolCall.id || '',
                    type: 'function',
                    function: {
                        name: toolCall.function?.name || '',
                        arguments: toolCall.function?.arguments || ''
                    }
                };

                // 确保索引位置有效
                while (toolCalls.length <= toolCall.index) {
                    toolCalls.push(null);
                }

                toolCalls[toolCall.index] = currentToolCall;
                debugLog('log', `创建工具调用 [${toolCall.index}]:`, currentToolCall);
            }
            // 更新现有工具调用
            else {
                currentToolCall = toolCalls[toolCall.index];

                // 更新函数名称（如果提供）
                if (toolCall.function?.name) {
                    currentToolCall.function.name = toolCall.function.name;
                }

                // 追加参数（如果提供）
                if (toolCall.function?.arguments) {
                    currentToolCall.function.arguments = (currentToolCall.function.arguments || '') +
                        toolCall.function.arguments;
                }

                // 更新ID（如果提供）
                if (toolCall.id) {
                    currentToolCall.id = toolCall.id;
                }

                debugLog('log', `更新工具调用 [${toolCall.index}]:`, currentToolCall);
            }
        }
        // 独立的参数片段（没有index）
        else if (toolCall.function?.arguments) {
            // 尝试更新最后一个工具调用
            if (toolCalls.length > 0) {
                const lastIdx = toolCalls.findIndex(t => t !== null);
                if (lastIdx >= 0) {
                    const lastCall = toolCalls[lastIdx];
                    lastCall.function.arguments = (lastCall.function.arguments || '') +
                        toolCall.function.arguments;
                    debugLog('log', `追加参数到工具调用 [${lastIdx}]:`, toolCall.function.arguments);
                }
            }
        }
    }
}

/**
 * 执行工具调用
 * @param {Array} toolCalls - 工具调用数组
 * @returns {Array} 执行结果数组
 */
async function executeToolCalls(toolCalls) {
    debugLog('log', '执行工具调用:', toolCalls);

    const results = [];

    // 过滤掉null和未完成的工具调用
    const validToolCalls = toolCalls.filter(call =>
        call !== null && call.function && call.function.name
    );

    debugLog('log', '有效的工具调用:', validToolCalls);

    for (const toolCall of validToolCalls) {
        try {
            const functionName = toolCall.function.name;
            let functionArgs = {};

            // 确保函数存在
            if (typeof functions[functionName] !== 'function') {
                debugLog('error', `函数 ${functionName} 未定义`);
                results.push({
                    tool_call_id: toolCall.id,
                    role: 'tool',
                    content: JSON.stringify({
                        status: 'error',
                        message: `函数 ${functionName} 未定义`
                    })
                });
                continue;
            }

            // 解析参数
            if (toolCall.function.arguments) {
                try {
                    // 首先尝试直接解析
                    functionArgs = JSON.parse(toolCall.function.arguments);
                } catch (parseError) {
                    debugLog('warn', 'JSON解析失败，尝试清理:', parseError);
                    // 清理常见的格式问题
                    let cleanArgs = toolCall.function.arguments
                        .replace(/\n/g, ' ')
                        .replace(/\s+/g, ' ')
                        .replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2":')
                        .replace(/:\s*'([^']*)'/g, ':"$1"')
                        .replace(/,\s*}/g, '}')
                        .replace(/,\s*\]/g, ']');

                    try {
                        functionArgs = JSON.parse(cleanArgs);
                    } catch (e) {
                        debugLog('error', '清理后解析仍失败:', e);
                        // 尝试使用专门的解析函数
                        functionArgs = parseFunctionArguments(functionName, toolCall.function.arguments);
                    }
                }
            }

            // 对非对象的参数进行处理
            if (typeof functionArgs !== 'object' || functionArgs === null) {
                functionArgs = {};
            }

            // 执行函数
            debugLog('log', `执行函数 ${functionName}:`, functionArgs);
            const result = await functions[functionName](functionArgs);

            // 添加结果
            results.push({
                tool_call_id: toolCall.id,
                role: 'tool',
                content: JSON.stringify(result)
            });

            debugLog('log', `函数 ${functionName} 执行结果:`, result);
        } catch (error) {
            debugLog('error', '执行函数调用时出错:', error);
            results.push({
                tool_call_id: toolCall.id,
                role: 'tool',
                content: JSON.stringify({
                    status: 'error',
                    message: `执行出错: ${error.message || '未知错误'}`
                })
            });
        }
    }

    return results;
}

/**
 * 聊天完成函数 - 流式响应
 * @param {Object} params - 参数对象
 * @param {Array} params.messages - 消息历史
 * @param {String} params.model - 模型名称
 * @param {Boolean} params.stream - 是否流式响应
 * @param {AbortSignal} params.signal - 中止信号
 * @param {Function} params.onChunk - 处理响应块的回调函数
 * @param {Function} params.onToolCall - 处理工具调用的回调函数
 * @param {Function} params.onToolCallEnd - 工具调用结束的回调函数
 * @param {Function} params.onToolCallStarted - 工具调用开始前的回调函数
 * @param {Function} params.onToolCallFinished - 工具调用完全结束后的回调函数
 * @param {Function} params.onError - 错误处理回调函数
 * @param {Function} params.onFinish - 完成回调函数
 */
async function streamChatCompletion(params) {
    const {
        messages,
        model = 'qwen-plus',
        stream = true,
        signal,
        onChunk,
        onToolCall,
        onToolCallEnd,
        onToolCallStarted,
        onToolCallFinished,
        onError,
        onFinish
    } = params;

    debugLog('info', '开始处理流式响应');
    const startTime = Date.now();
    let firstTokenReceived = false;

    // 工具调用相关状态
    let toolCalls = [];
    let currentToolCall = null;
    let systemPrompt = params.systemPrompt || null;

    try {
        // 准备消息上下文
        let contextMessages = [...messages];  // 复制一份以避免修改原始数据

        // 如果提供了系统提示，添加到消息开头
        if (systemPrompt) {
            contextMessages = [
                { role: 'system', content: systemPrompt },
                ...contextMessages
            ];
        }

        // 获取当前模型对应的OpenAI实例
        const currentOpenAI = getOpenAIInstance(model);

        // 工具调用结果追踪
        const toolResults = [];
        let conversationFinished = false;

        // 函数用于执行单次对话请求并处理结果
        async function executeConversationTurn(currentMessages) {
            debugLog('log', '执行对话请求，消息数量:', currentMessages.length);

            toolCalls = [];  // 重置工具调用收集数组

            // 调用 API 接口，设置流式响应和函数调用
            const response = await currentOpenAI.chat.completions.create({
                model: model,
                messages: currentMessages,
                stream: stream,
                tools: functionDefinitions,
                tool_choice: "auto"
            }, {
                signal: signal
            });

            debugLog('log', 'OpenAI API调用成功，开始处理响应流');

            // 用于收集非工具调用的响应内容
            let responseContent = '';
            let finishReason = null;
            let hasDetectedToolCall = false;

            // 流式响应逐步更新
            for await (const chunk of response) {
                // 记录首包时间
                if (!firstTokenReceived) {
                    firstTokenReceived = true;
                    const firstTokenTime = Date.now() - startTime;
                    debugLog('info', `收到首个token，耗时: ${firstTokenTime}ms`);
                }

                // 处理函数调用
                const deltaToolCalls = chunk.choices[0]?.delta?.tool_calls;
                if (deltaToolCalls && deltaToolCalls.length > 0) {
                    // 检测到第一次工具调用时触发开始回调
                    if (!hasDetectedToolCall && onToolCallStarted) {
                        hasDetectedToolCall = true;
                        debugLog('info', '检测到工具调用，触发工具调用开始回调');
                        await onToolCallStarted();
                    }

                    debugLog('log', '检测到工具调用请求:', deltaToolCalls);
                    await handleToolCalls(deltaToolCalls, toolCalls, currentToolCall);

                    // 通知上层处理工具调用
                    if (onToolCall) {
                        onToolCall(deltaToolCalls, toolCalls);
                    }
                }

                // 获取完成原因（如果有）
                finishReason = chunk.choices[0]?.finish_reason;

                // 处理普通文本内容
                const content = chunk.choices[0]?.delta?.content || '';
                if (content) {
                    responseContent += content;
                    if (onChunk) {
                        await onChunk(content, chunk);
                    }
                }
            }

            // 如果是因为工具调用而结束，执行工具并返回结果
            if (finishReason === 'tool_calls' && toolCalls.length > 0) {
                debugLog('info', '回合因工具调用而结束，执行工具调用');

                if (onToolCallEnd) {
                    const results = await executeToolCalls(toolCalls);
                    onToolCallEnd(toolCalls, results);

                    // 存储工具调用结果
                    results.forEach(result => toolResults.push(result));

                    // 添加工具调用和结果到消息上下文
                    const assistantMsg = {
                        role: 'assistant',
                        content: responseContent || null  // 保留推理内容
                    };

                    // 只有当工具调用非空时才添加tool_calls字段
                    const validToolCalls = toolCalls.filter(tc => tc !== null);
                    if (validToolCalls.length > 0) {
                        assistantMsg.tool_calls = validToolCalls.map(tc => ({
                            id: tc.id,
                            type: 'function',
                            function: {
                                name: tc.function.name,
                                arguments: tc.function.arguments
                            }
                        }));
                    }

                    currentMessages.push(assistantMsg);

                    // 添加工具结果到消息上下文
                    for (const result of results) {
                        // 确保工具调用结果不会包含AI回复
                        if (result && result.content) {
                            // 检查结果是否为有效的JSON
                            let cleanContent = result.content;
                            try {
                                // 尝试解析JSON，如果成功说明这是一个工具结果
                                JSON.parse(cleanContent);
                                // 格式正确，直接使用
                                currentMessages.push({
                                    role: 'tool',
                                    tool_call_id: result.tool_call_id,
                                    content: cleanContent
                                });
                            } catch (e) {
                                // 处理可能包含AI回复的工具结果
                                // 尝试从字符串中提取有效的JSON部分
                                const jsonEndIndex = cleanContent.indexOf('"}') + 2;
                                if (jsonEndIndex > 1 && jsonEndIndex < cleanContent.length - 1) {
                                    // 分离工具结果和AI回复
                                    const toolResult = cleanContent.substring(0, jsonEndIndex);
                                    // 提取AI回复部分添加到responseContent
                                    const aiResponse = cleanContent.substring(jsonEndIndex).trim();
                                    if (aiResponse) {
                                        responseContent += aiResponse;
                                    }

                                    // 只将工具结果部分添加到工具消息
                                    currentMessages.push({
                                        role: 'tool',
                                        tool_call_id: result.tool_call_id,
                                        content: toolResult
                                    });
                                } else {
                                    // 无法分离，使用原始内容
                                    currentMessages.push({
                                        role: 'tool',
                                        tool_call_id: result.tool_call_id,
                                        content: cleanContent
                                    });
                                }
                            }
                        } else {
                            // 没有内容的情况
                            currentMessages.push({
                                role: 'tool',
                                tool_call_id: result.tool_call_id,
                                content: result.content || ""
                            });
                        }
                    }

                    // 所有工具调用执行完毕后触发完成回调
                    if (onToolCallFinished) {
                        debugLog('info', '所有工具调用执行完毕，触发工具调用完成回调');
                        await onToolCallFinished(results);
                    }

                    // 递归继续对话，直到没有更多工具调用
                    return executeConversationTurn(currentMessages);
                }
            } else {
                // 常规文本回复结束，对话完成
                debugLog('info', '对话完成，完成原因:', finishReason);
                conversationFinished = true;

                // 如果有最终的文本内容，添加到上下文（可选，用于连续对话场景）
                if (responseContent) {
                    currentMessages.push({
                        role: 'assistant',
                        content: responseContent
                    });
                }

                // 返回最终结果，包含所有工具调用和最终文本
                return {
                    content: responseContent,
                    toolResults,
                    finishReason,
                    hasToolCalls: toolResults.length > 0
                };
            }
        }

        // 开始执行对话
        const result = await executeConversationTurn(contextMessages);

        // 对话已完全结束，调用完成回调
        if (conversationFinished && onFinish) {
            onFinish(result);
        }

        debugLog('info', '流式响应处理完成');
    } catch (error) {
        debugLog('error', '流式响应处理出错:', error);
        if (onError) {
            onError(error);
        }
    }
}

/**
 * 处理函数调用结果，生成最终回复
 * @param {Array} results - 函数调用结果
 * @param {String} originalQuery - 原始查询
 * @param {String} selectedModel - 选择的模型
 * @param {Function} handleResponseChunk - 处理响应块的回调函数
 * @returns {Promise<String>} 完整的响应文本
 */
async function processFunctionResults(results, originalQuery, selectedModel, handleResponseChunk) {
    debugLog('log', '处理函数调用结果，生成最终响应');

    // 准备上下文消息
    const messages = [
        { role: 'user', content: originalQuery }
    ];

    // 添加函数调用输出
    results.forEach(result => {
        // 确保消息格式符合OpenAI API要求，并且只有在有效的tool_call_id时才添加工具调用
        debugLog('log', '处理函数执行结果:', result);
        if (result.tool_call_id) {
            messages.push({
                role: 'assistant',
                content: null,
                tool_calls: [{
                    id: result.tool_call_id,
                    type: 'function',
                    function: {
                        name: 'function_call',
                        arguments: typeof result.content === 'string' ? result.content : JSON.stringify(result.content)
                    }
                }]
            });
            messages.push({
                role: 'tool',
                tool_call_id: result.tool_call_id,
                content: typeof result.content === 'string' ? result.content : JSON.stringify(result.content)
            });
        }
    });

    debugLog('log', '发送给模型的消息:', JSON.stringify(messages));

    // 获取当前模型对应的OpenAI实例
    const currentOpenAI = getOpenAIInstance(selectedModel);

    // 添加系统提示，指导模型如何处理结果
    messages.unshift({
        role: 'system',
        content: '你是一个智能助手，负责帮助用户理解从数据库中获取的信息，并以自然语言进行回答。请使用自然、口语化的语言回答，不要使用markdown、列表或其他格式化文本。像正常对话一样用简单直接的语言表达，避免使用"以下是"、"总结"等书面语言风格。保持温暖亲切的语气，就像在面对面交谈一样。需要时，你可以调用更多工具进行深入分析。'
    });

    // 允许再次进行工具调用
    return await new Promise((resolve, reject) => {
        let fullResponse = '';

        streamChatCompletion({
            messages: messages,
            model: selectedModel,
            onChunk: async (content) => {
                fullResponse += content;
                if (handleResponseChunk) {
                    await handleResponseChunk(content);
                }
            },
            onToolCall: (deltaToolCalls, toolCalls) => {
                // 这里可以处理二次工具调用
                debugLog('log', '处理二次工具调用:', deltaToolCalls);
            },
            onToolCallEnd: async (toolCalls, results) => {
                // 处理二次工具调用结果
                debugLog('log', '处理二次工具调用结果:', results);
            },
            onError: (error) => {
                debugLog('error', '处理函数结果失败:', error);
                reject(error);
            },
            onFinish: (result) => {
                debugLog('log', '最终完整响应:', fullResponse);
                resolve(fullResponse);
            }
        });
    });
}

/**
 * 获取上下文消息（从消息历史中提取）
 * @param {Array} messages - 消息数组
 * @param {Number} maxContextPairs - 最大上下文对数量
 * @returns {Array} 处理后的上下文消息
 */
function getContextMessages(messages, maxContextPairs = 10) {
    const reversedMessages = [...messages].reverse();
    let context = [];
    let lastRole = null;
    let msgCount = 0;

    // 步骤1：逆序收集消息，跳过连续相同角色
    for (const msg of reversedMessages) {
        // 添加消息到上下文，保留工具调用信息
        if (msg.role !== lastRole) {
            // 如果是带有工具调用的消息，保留完整结构，但确保tool_calls不为空数组
            if (msg.tool_calls && msg.tool_calls.length > 0) {
                context.unshift({
                    role: msg.role,
                    content: msg.content || "", // 确保content不为null或undefined
                    tool_calls: msg.tool_calls
                });
            }
            // 如果是工具响应消息，保留完整结构
            else if (msg.role === 'tool') {
                context.unshift({
                    role: msg.role,
                    tool_call_id: msg.tool_call_id,
                    content: msg.content || "" // 确保content不为null或undefined
                });
            }
            // 普通消息
            else {
                context.unshift({
                    role: msg.role,
                    content: msg.content || "" // 确保content不为null或undefined
                });
            }
            lastRole = msg.role;

            // 如果是用户消息或助手消息，增加计数
            if (msg.role === 'user' || msg.role === 'assistant') {
                msgCount++;
                // 达到最大组数限制后停止
                if (msgCount >= maxContextPairs * 2) break;
            }
        }
    }

    // 步骤2：确保第一条非系统消息是用户消息
    const firstNonSystemIndex = context.findIndex(m => m.role !== 'system');
    if (firstNonSystemIndex !== -1 && context[firstNonSystemIndex].role !== 'user') {
        const firstUserIndex = context.slice(firstNonSystemIndex).findIndex(m => m.role === 'user');
        if (firstUserIndex === -1) {
            // 如果没有用户消息，添加一个默认的用户消息
            context = [{
                role: 'user',
                content: '请继续'
            }];
        } else {
            context = context.slice(firstNonSystemIndex + firstUserIndex);
        }
    }

    // 确保返回的消息数组非空
    if (context.length === 0) {
        context = [{
            role: 'user',
            content: '请继续'
        }];
    }

    return context;
}

// 导出所有需要的函数和配置
export default {
    openaiConfigs,
    getOpenAIInstance,
    parseFunctionArguments,
    handleToolCalls,
    executeToolCalls,
    processFunctionResults,
    getContextMessages,
    streamChatCompletion
}; 
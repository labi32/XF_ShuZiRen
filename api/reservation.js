// 获取预约历史列表
export function getReservationHistory(data) {
	return uni.request({
		url: '/api/reservation/history',
		method: 'GET',
		data
	})
} 
/**
 * 平台
 */
enum PLANTFORM {
	WINDOWS,
	ANDROID,
	IOS,
	UNI,
	ALL,//此处是为了多平台
	DAMPAGE//此处是为了自定义页面
}

/**
 * 服务名称
 */
enum SERVICE_NAME {
	DamRouterService = "DamRouterService",
	DamPageService = "DamPageService",
	DamPhoneAbilityService = "DamPhoneAbilityService",
}
export {
	PLANTFORM,
	SERVICE_NAME
}
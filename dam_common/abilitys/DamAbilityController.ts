/**
 * DamAbilityService主要是应用于面对不同平台下的页面交互处理
 */
import { IReceiveObserver } from '../hybird/HyBirdServer';
import HttpProcessor from '../respository/http/dam-http';
import { PLANTFORM, SERVICE_NAME } from '@/dam_common/abilitys/Constants';
import IDamAbilityController from '@/dam_common/abilitys/interface/IDamAbilityController';
import IDamBaseBridgeService from '@/dam_common/abilitys/interface/IDamBaseBridgeService';
import { IDamPageService } from '@/dam_common/abilitys/interface/IDamPageService';
import { IDamPhoneAbilityService } from '@/dam_common/abilitys/interface/IDamPhoneAbilityService';
import { IDamRouterService, IDamRouteOptions } from '@/dam_common/abilitys/interface/IDamRouterService';

export default class DamAbilityController implements IDamAbilityController {
	executor: Executor;
	interceptors: IDamBaseBridgeService[];
	httpPrecessors: HttpProcessor[];
	constructor() {
		this.interceptors = new Array();
		this.httpPrecessors = new Array();
		this.executor = new Executor(this);
	}

	getPlantform(): PLANTFORM {
		return PLANTFORM.ALL;
	}

	getSoftIndex(): number {
		return -1;
	}
	/**
	 * 注册拦截器
	 */
	addService(interceptor: IDamBaseBridgeService) {
		this.interceptors.push(interceptor);
		this.interceptors.sort((first, second) => second.getSoftIndex() - first.getSoftIndex());
	}
	/**
	 * 注册拦截器
	 */
	addServices(...interceptors: IDamBaseBridgeService[]) {
		this.interceptors = [...this.interceptors, ...interceptors];
		this.interceptors.sort((first, second) => second.getSoftIndex() - first.getSoftIndex());
	}
	/**
	 * 注册http请求执行器
	 */
	addHttpProcessor(httpPrecessor: HttpProcessor) {
		this.httpPrecessors.push(httpPrecessor);
	}
	/**
	 * 注册http请求执行器
	 */
	addHttpProcessors(...httpPrecessors: HttpProcessor[]) {
		this.httpPrecessors = [...this.httpPrecessors, ...httpPrecessors];
	}
	/**
	 * 获取http请求执行器
	 */
	getHttpProcessor(key: string): HttpProcessor {
		let httpPrecessors = this.httpPrecessors;
		for (let i = 0; i <= httpPrecessors.length; i++) {
			let httpPrecessor = httpPrecessors[i];
			if (httpPrecessor.key === key) {
				return httpPrecessor;
			}
		}
	}
}

/**
 * 执行器
 */
export class Executor implements IDamRouterService, IDamPageService, IDamPhoneAbilityService {
	controller: DamAbilityController;
	constructor(controller: DamAbilityController) {
		this.controller = controller;
	}
	getRouterParams(page: any, option: any) {
		var func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).getRouterParams(page, option);
			}
		};
		return this.execute(func);
	}
	redirectTo(options: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).redirectTo(options);
			}
		};
		return this.execute(func);
	}
	reLaunch(options: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).reLaunch(options);
			}
		};
		return this.execute(func);
	}
	navigateTo(options: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).navigateTo(options);
			}
		};
		return this.execute(func);
	}
	navigateback(options?: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).navigateback(options);
			}
		};
		return this.execute(func);
	}
	refresh(options: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).refresh(options);
			}
		};
		return this.execute(func);
	}
	refreshBack(options: IDamRouteOptions): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamRouterService) {
				return (interceptor as IDamRouterService).refreshBack(options);
			}
		};
		return this.execute(func);
	}
	showToast(page: any, text: string, plantform?: PLANTFORM): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamPageService) {
				return (interceptor as IDamPageService).showToast(page, text, plantform);
			}
		};
		return this.execute(func);
	}
	scanQrcode(page: any, plantform?: PLANTFORM): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamPhoneAbilityService) {
				return (interceptor as IDamPhoneAbilityService).scanQrcode(page, plantform);
			}
		};
		return this.execute(func);
	}
	registPlantformPageCallback(page: any, key: string, callback: IReceiveObserver, plantform?: PLANTFORM): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamPhoneAbilityService) {
				return (interceptor as IDamPhoneAbilityService).registPlantformPageCallback(
					page,
					key,
					callback,
					plantform
				);
			}
		};
		return this.execute(func);
	}
	unregistPlantformPageCallback(page: any, key: string, plantform?: PLANTFORM): boolean {
		let func = function (interceptor: IDamBaseBridgeService) {
			if (interceptor.getServiceName() === SERVICE_NAME.DamPhoneAbilityService) {
				return (interceptor as IDamPhoneAbilityService).unregistPlantformPageCallback(page, key, plantform);
			}
		};
		return this.execute(func);
	}
	getPlantform(): PLANTFORM {
		return;
	}
	getServiceName(): SERVICE_NAME {
		return;
	}
	getSoftIndex(): number {
		return;
	}

	execute(func: Function) {
		let interceptors = this.controller.interceptors;
		for (let i = 0; i <= interceptors.length; i++) {
			let interceptor = interceptors[i];
			if (interceptor) {
				let result = func(interceptor);
				if (result) {
					return result;
				}
			}
		}
		return false;
	}
}

/**
 * 控制集合底座，包括不同控制器，比如页面控制器（同一页面组件在不同平台的体现），方法控制器（不同平台的兼容）等
 *
 */
import IDamAbilityController from '@/dam_common/abilitys/interface/IDamAbilityController';

export default class DamBridge {
	public abilityController: IDamAbilityController;

	//多个app实例 TODO 测试跨线程问题
	public static APPS = [];

	private constructor() {}

	/**
	 * 注册能力控制器
	 */
	registDamAbilityController(abilityController: IDamAbilityController) {
		this.abilityController = abilityController;
	}

	/**
	 * 绑定APP
	 * Tip：
	 * 1）因为在组件中无法使用App，H5平台可能也会有问题，所以使用自己挂载的app。
	 * 2）如果是多个App实例则会有跨线程的问题，要注意
	 */
	bindApp(key: string, app: any) {
		app.$damBridge = this;
		// app.config.globalProperties.$damBridge = this
		DamBridge.APPS.push({
			key: key,
			app: app
		});
	}

	/**
	 * 获取controllers
	 * @param app
	 * @return
	 */
	static getBridge(key: string): DamBridge {
		// return app.$damBridge
		let app = DamBridge.getApp(key);
		if (app) {
			return app.$damBridge;
		}
		return null;
	}

	/**
	 * 获取AbilityService
	 * @param key
	 * @return
	 */
	static getDamAbilityController(key: string): IDamAbilityController {
		return DamBridge.getBridge(key).abilityController;
	}
	/**
	 * 获取app实例
	 * @param  key
	 */
	static getApp(key: string) {
		for (let i = 0; i < DamBridge.APPS.length; i++) {
			if (DamBridge.APPS[i].key == key) {
				return DamBridge.APPS[i].app;
			}
		}
	}

	/**
	 * 构造器
	 */
	static Builder = class Builder {
		bridge: DamBridge;
		constructor() {
			this.bridge = new DamBridge();
		}

		build(): DamBridge {
			return this.bridge;
		}
	};
}

import { PLANTFORM, SERVICE_NAME } from "@/dam_common/abilitys/Constants";

interface IDamBaseBridgeService {
	/**
	 * 用于获取当前的服务可用平台
	 */
	getPlantform() : PLANTFORM;

	/**
	 * 获取排序权重，数字越大，越靠前。
	 */
	getSoftIndex() : number;

	/**
	 * 获取服务名称。
	 */
	getServiceName() : SERVICE_NAME;
}
export default IDamBaseBridgeService
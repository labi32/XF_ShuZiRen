/**
 * 用作页面控制的服务
 */
import { PLANTFORM } from "../Constants";
import IDamBaseBridgeService from "./IDamBaseBridgeService";

//TODO 弹窗类跳页，动画，如评论功能，详见 navigateback 的 animationType
interface IDamPageService extends IDamBaseBridgeService {

	/**
	 * 展示Toast
	 * @param text 展示的文字 
	 * @param plantform 平台
	 * @returns 如果是true则消化了此方法
	 */
	showToast(page : any, text : string, plantform ?: PLANTFORM) : boolean;

}

export {
	IDamPageService
};
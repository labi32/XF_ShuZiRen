/**
 * 用作终端能力控制的服务
 */
import { IReceiveObserver } from "@/dam_common/hybird/HyBirdServer";
import { PLANTFORM } from "../Constants";
import IDamBaseBridgeService from "./IDamBaseBridgeService";

interface IDamPhoneAbilityService extends IDamBaseBridgeService {
	/**
	 * 注册平台回调观察者
	 */
	registPlantformPageCallback(page : any, key : string, callback : IReceiveObserver, plantform ?: PLANTFORM) : boolean;
	/**
	 * 注销平台回调观察者
	 */
	unregistPlantformPageCallback(page : any, key : string, plantform ?: PLANTFORM) : boolean;
	/**
	 * 扫描二维码
	 */
	scanQrcode(page : any, plantform ?: PLANTFORM) : boolean;

}

export {
	IDamPhoneAbilityService
};
/**
 * 用作页面控制的服务
 */
import { PLANTFORM } from '../Constants';
import IDamBaseBridgeService from './IDamBaseBridgeService';

//TODO 弹窗类跳页，动画，如评论功能，详见 navigateback 的 animationType
interface IDamRouterService extends IDamBaseBridgeService {
	/**
	 * 获取跳转页面参数
	 */
	getRouterParams(page: any, options: any): {};
	/**
	 * 跳转
	 */
	navigateTo(options: IDamRouteOptions): boolean;

	/**
	 * 跳转
	 */
	redirectTo(options: IDamRouteOptions): boolean;

	/**
	 * 跳转, 清除历史路由
	 */
	reLaunch(options: IDamRouteOptions): boolean;

	/**
	 * 返回
	 */
	navigateback(options?: IDamRouteOptions): boolean;

	/**
	 * 刷新
	 */
	refresh(options: IDamRouteOptions): boolean;

	/**
	 * 返回刷新
	 */
	refreshBack(options: IDamRouteOptions): boolean;
}

/**
 * 路由配置
 */
interface IDamRouteOptions {
	/**
	 * 页面对象，极少使用
	 */
	page?: any;
	/**
	 * 跳转路径
	 */
	path?: string;
	/**
	 * 需要鉴权，此属性需配合鉴权逻辑使用,此鉴权优先级高于page.json配置的鉴权
	 */
	needAuth?: boolean;
	/**
	 * 页面参数
	 */
	data?: any;
	/**
	 * 预留的平台参数
	 */
	plantform?: PLANTFORM;
	/**
	 * 页面事件
	 */
	events?: any;
	/**
	 * 是否刷新当前页面
	 */
	isReload?: boolean;
	/**
	 * 刷新事件
	 */
	reloadFunc?: Function;
}
export { IDamRouterService, IDamRouteOptions };

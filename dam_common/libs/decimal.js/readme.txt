用于浮点型的数值计算，解决可能出现的多位小数问题。
案例：
import {Decimal} from '@/dam_common/libs/decimal.js';		//注意此处引入的是文件夹。

let a = new Decimal('122.711111');
let b = new Decimal('192.1111');
let sum = a.plus(b).toString();
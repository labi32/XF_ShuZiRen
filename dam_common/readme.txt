dam_common主要是为了一些公用组件、方法的知识库，不包括逻辑处理，不包括整体框架


注意：
1、组件注册：
	"easycom": {
		"autoscan": true,
		"custom": {
			"^dam-(.*)": "@/dam_common/components/view/dam-$1.vue"// 匹配dam_common中components目录内的vue文件，自动挂载到全局组件
		}
	},
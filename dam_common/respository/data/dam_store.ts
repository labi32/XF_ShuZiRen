/**
 * 保存内容
 * @param {String} key String标识
 * @param {any} value 值
 */
function save(key, value) {
	uni.setStorageSync(key, value)
}

/**
 * 读取内容
 * @param {String} key String标识
 */
function get(key) {
	let value = uni.getStorageSync(key)
	return value;
}

/**
 * 移除内容
 * @param {Object} key String标识
 */
function remove(key) {
	return uni.removeStorageSync(key)
}
export default {
	save,
	get,
	remove
}
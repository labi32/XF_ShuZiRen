/**
 * Http核心执行类。
 */
import FormData from "./dam-form-data";
import LogUtil from "@/dam_common/utils/LogUtil";

export default class HttpProcessor {
	//http执行器标识
	key : string = ''
	requestInterceptorFunc;
	responseInterceptorFunc;
	failInterceptorFunc;
	constructor(key : string) {
		this.key = key;
	}
	/**
	 * 注册一个Http请求前的拦截器
	 * @param {*} resFunc 拦截器方法，参数为http的响应体，返回一个请求体对象
	 */
	registRequestInterceptor(resFunc) {
		this.requestInterceptorFunc = resFunc
	}
	/**
	 * 注册一个Http响应后的拦截器
	 * @param {*} resFunc 拦截器方法，参数为http的响应体，返回一个响应体对象
	 */
	registResponseInterceptor(resFunc) {
		this.responseInterceptorFunc = resFunc
	}
	/**
	 * 注册一个Http响应失败后的拦截器
	 * @param {*} resFunc 拦截器方法，参数为http的异常信息，返回一个err对象
	 */
	registFailInterceptor(resFunc) {
		this.failInterceptorFunc = resFunc
	}
	// TODO 增加一个文件上传的
	/**
	 * 请求表单
	 * @param {*} ip 
	 * @param {*} data 
	 */
	postForm(ip, data, header = null) {
		LogUtil.printInfo("------------Http 请求内容 postForm-------------");
		LogUtil.printInfo(ip);
		LogUtil.printInfo(data);
		let formData = new FormData();
		for (const [key, value] of Object.entries(data)) {
			formData.append(key, value);
		}
		var realData = formData.getData();
		let formHeader = {
			'content-type': realData.contentType
		};
		let finalHeader = formHeader;
		if (header) {
			finalHeader = Object.assign(formHeader, header)
		}
		return this.post(ip, realData.buffer, finalHeader);
	}

	/**
	 * Json方式的post
	 * @param {*} ip 
	 * @param {*} json 
	 */
	postJson(ip, json, header = null) {
		LogUtil.printInfo("------------Http 请求内容 postJson-------------")
		LogUtil.printInfo(ip);
		LogUtil.printInfo(json);
		let formHeader = {
			'content-type': 'application/json; charset=UTF-8'
		}
		let finalHeader = formHeader;
		if (header) {
			finalHeader = Object.assign(formHeader, header)
		}
		return this.post(ip, json, finalHeader);
	}
	/**
	 * post方法实现
	 * @param {*} url 
	 * @param {*} data 
	 * @param {*} header 
	 */
	post(url, data, header) {
		var _this = this
		return new Promise((resole, reject) => {
			var requestBody = {
				url: url, // 请求的URL  
				method: 'POST', // 请求方法  
				data: data, // 要提交的数据 
				header: header,
				enableCookie: true,
				success: function (res) {
					LogUtil.printInfo("------------Http 响应体 -------------")
					LogUtil.printInfo(res);
					var finalRes = res
					if (_this.responseInterceptorFunc) {
						finalRes = _this.responseInterceptorFunc(res);
					}
					resole(finalRes);
				},
				fail: function (err) {
					var finalErr = err
					if (_this.failInterceptorFunc) {
						finalErr = _this.failInterceptorFunc(err);
					}
					// var finalRes = _this.parseErrorResonse('接口调用失败');
					reject(finalErr);
				}
			}
			if (_this.requestInterceptorFunc) {
				requestBody = _this.requestInterceptorFunc(requestBody)
			}
			uni.request(requestBody)
		})
	}
}
import {
	PLANTFORM
} from "@/dam_common/abilitys/Constants";

/**
 * vmin 转换成px
 * @param {Object} vminValue
 */
function vminToPx(vminValue) {
	let systemInfo = uni.getSystemInfoSync()
	// 获取viewport的宽度和高度
	const vw = systemInfo.windowHeight;
	const vh = systemInfo.windowWidth;
	// 计算vmin中的较小值
	const smallest = Math.min(vw, vh);
	// 将vmin值转换为px
	return smallest * vminValue / 100;
}

/**
 * 获取当前平台
 */
function getPlantform() {
	return new Promise((resolve, reject) => {
		try {
			let info = uni.getSystemInfoSync();
			let pl = PLANTFORM.WINDOWS;
			if (info.platform == 'android') {
				// Android 平台的处理逻辑
				pl = Plantform.ANDROID;
			} else if (info.platform == 'ios') {
				// iOS 平台的处理逻辑
				pl = Plantform.IOS;
			} else if (info.platform == 'windows') {
				// Windows平台的处理逻辑
				pl = Plantform.WINDOWS;
			}
			resolve(pl)
		} catch (e) {
			reject(e)
		}
	})
}

/**
 * 获取当前平台
 */
function getPlantformSync() {
	let info = uni.getSystemInfoSync();
	let pl = PLANTFORM.WINDOWS;
	if (info.platform == 'android') {
		// Android 平台的处理逻辑
		pl = PLANTFORM.ANDROID;
	} else if (info.platform == 'ios') {
		// iOS 平台的处理逻辑
		pl = PLANTFORM.IOS;
	} else if (info.platform == 'windows') {
		// Windows平台的处理逻辑
		pl = PLANTFORM.WINDOWS;
	}
	return pl
}

/**
 * 获取toolbar高度
 * @param {Object} callback
 */
function getToolbarHeight(callback) {
	let func = function(height) {
		callback(44 + height)
	};
	getStatusHeight(func);
}
/**
 * 获取状态栏高度
 * @param {Object} callback
 */
function getStatusHeight(callback) {
	uni.getSystemInfo({
		success: res => {
			let statusBarHeight = res.statusBarHeight
			if (res.platform === 'android') {
				statusBarHeight = 24
			} else if (res.platform === 'ios') {
				statusBarHeight = 20
			}
			let finalHeight = statusBarHeight;
			callback(finalHeight)
		}
	});
}

/**
 * 获取安全区域
 * @param {Object} callback
 */
function getSafeArea(callback) {
	callback(uni.getWindowInfo().safeArea);
	// uni.getSystemInfo({
	// 	success:res=>{			
	// 		callback(res.safeArea)
	// 	}
	// })
}
/**
 * 获取底部导航高度
 * @param {Object} callback
 */
function getNavigationBarHeight(callback) {
	uni.getSystemInfo({
		success: res => {
			let height = res.screenHeight - res.windowHeight;
			callback(height)
		}
	});
}
/**
 * 获取安全区域底部的高度
 * @param {Object} callback
 */
function getSafeAreaBottomH(callback) {
	const _callback = function(safeArea) {
		callback(uni.getWindowInfo().windowHeight - safeArea.bottom);
	};
	getSafeArea(_callback);
}
export default {
	vminToPx,
	getPlantformSync,
	getPlantform,
	getToolbarHeight,
	getStatusHeight,
	getNavigationBarHeight,
	getSafeArea,
	getSafeAreaBottomH
}
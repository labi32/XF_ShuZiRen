/**
 * 获取当前的时间字符串
 * @param {*} date 
 */
const getCurTimeStr = date => {
	const year = date.getFullYear();
	const month = date.getMonth() + 1;
	const day = date.getDate();
	const hour = date.getHours();
	const minute = date.getMinutes();
	const second = date.getSeconds();

	return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatTimeNumber).join(':')}`;
}

/**
 * 对数值类型的时间进行格式化，如hour是 3，格式化后为 03
 * @param {*} n 
 */
const formatTimeNumber = n => {
	n = n.toString();
	return n[1] ? n : `0${n}`;
}

/**
 * 格式化时间
 * @param {*} time 秒
 */
const formatTime = time => {
	if (typeof time !== 'number' || time < 0) {
		return time
	}

	const hour = parseInt(time / 3600, 10)
	time %= 3600
	const minute = parseInt(time / 60, 10)
	time = parseInt(time % 60, 10)
	const second = time

	return ([hour, minute, second]).map(formatTimeNumber).join(':')
}
function dateFormat(fmt, date) {
	let o = {
		"M+": date.getMonth() + 1, //月份
		"d+": date.getDate(), //日
		"H+": date.getHours(), //小时
		"m+": date.getMinutes(), //分
		"s+": date.getSeconds(), //秒
		"q+": Math.floor((date.getMonth() + 3) / 3), //季度
		"S": date.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (let k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : ((
			"00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}
export {
	getCurTimeStr,
	formatTimeNumber,
	formatTime,
	dateFormat
}
/**
 * 打印控制台日志
 * @param {any[]} txt
 */
function printInfo(txt) {
	if (process.env.NODE_ENV === 'development') {
		console.log(txt)
	}
}
export default {
	printInfo
}
/**
 * 控制浮点数位数
 * 使用示例 toFixed(3.14159, 2); // 结果为3.14
 * @param {Object} num
 * @param {Object} fixed
 */
function toFixed(num, fixed) {
	const re = new RegExp('^-?\\d+(?:\.\\d{0,' + fixed + '})?');
	return parseFloat((num || 0).toString().match(re)[0]);
}

export {
	toFixed
}
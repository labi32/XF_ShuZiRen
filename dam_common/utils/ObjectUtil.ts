/**
 * 监听数据变化
 * @param {Object} object 监听的对象实例
 * @param {String} propertyKey 监听对象中的属性名
 * @param {Function} func 监听回调function
 */
function watchDataChanged(object, propertyKey, func) {
	let val = object[propertyKey]
	Object.defineProperty(object, propertyKey, {
		set: function(value) {
			val = value
			func(propertyKey, value);
		},
		get: function() {
			return val;
		}
	})
}
// function unWatchGlobalDataChanged(propertyKey, func) {}

/**
 * 深拷贝
 * @param {Object} arr
 */
function deepCopyArray(arr) {
	return arr.map(item => {
		if (Array.isArray(item)) {
			return deepCopyArray(item);
		} else if (typeof item === 'object' && item !== null) {
			return deepCopyObject(item);
		}
		return item;
	});
}
/**
 * 深拷贝
 * @param {Object} obj
 */
function deepCopyObject(obj) {
	let copy = Object.create(Object.getPrototypeOf(obj));
	Object.keys(obj).forEach(key => {
		if (obj[key] instanceof Array) {
			copy[key] = deepCopyArray(obj[key]);
		} else if (typeof obj[key] === 'object' && obj[key] !== null) {
			copy[key] = deepCopyObject(obj[key]);
		} else {
			copy[key] = obj[key];
		}
	});
	return copy;
}

export default {
	watchDataChanged,
	deepCopyArray,
	deepCopyObject
}
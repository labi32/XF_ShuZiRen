/**
 * 注册h5的方向监听
 */
const registH5OrientationChangeListener = function(callback) {
	window.addEventListener("orientationchange", callback);
}

/**
 * 注销h5的方向监听
 */
const unregistH5OrientationChangeListener = function(callback) {
	window.removeEventListener("orientationchange", callback);
}

/**
 * 注册h5的窗口大小变化监听
 */
const registH5ResizeChangeListener = function(callback) {
	window.addEventListener("resize", callback);
}
/**
 * 注销h5的窗口大小变化监听
 */
const unregistH5ResizeChangeListener = function(callback) {
	window.removeEventListener("resize", callback);
}

export {
	registH5OrientationChangeListener,
	unregistH5OrientationChangeListener,
	registH5ResizeChangeListener,
	unregistH5ResizeChangeListener
}
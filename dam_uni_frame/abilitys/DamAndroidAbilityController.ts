/**
 *DamAndroidAbilityService主要是应用于Android平台的应用交互处理
 */
import DamAbilityController from '@/dam_common/abilitys/DamAbilityController';
import { PLANTFORM, SERVICE_NAME } from '@/dam_common/abilitys/Constants';
import { IDamPageService } from '@/dam_common/abilitys/interface/IDamPageService';
import { IDamRouterService, IDamRouteOptions } from '@/dam_common/abilitys/interface/IDamRouterService';
import { IDamPhoneAbilityService } from '@/dam_common/abilitys/interface/IDamPhoneAbilityService';
import HyBirdServer, { IReceiveObserver } from '@/dam_common/hybird/HyBirdServer';

/**
 * TODO 需要继续扩展功能 ,Android 的是不是也可以注册uni的服务?比如navigateTo 方法,待测试
 */
export default class DamAndroidAbilityController {
	registServices(controller: DamAbilityController) {
		controller.addServices(this.routerService, this.pageService, this.phoneAbilityService);
	}

	//Android的路由服务
	routerService: IDamRouterService = {
		getRouterParams: function (page: any, options: any): {} {
			//TODO 要做好方案 , 可以添加原生页跳转的控制
			return undefined;
		},
		navigateTo: function (options: IDamRouteOptions): boolean {
			uni.navigateTo({
				url: options.path //+ this._createPageParams(options.data)
			});
			return true;
		},
		redirectTo: function (options: IDamRouteOptions): boolean {
			uni.redirectTo({
				url: options.path //+ this._createPageParams(options.data)
			});
			// return true;
			return true;
		},
		reLaunch: function (options: IDamRouteOptions): boolean {
			return false;
		},
		navigateback: function (options: IDamRouteOptions): boolean {
			// uni.navigateBack()
			// return true;
			return false;
		},
		refresh: function (options: IDamRouteOptions): boolean {
			//TODO 此处暂时没想到怎么处理
			return false;
		},
		refreshBack: function (options: IDamRouteOptions): boolean {
			return false;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.ANDROID;
		},
		getSoftIndex: function (): number {
			return 40;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamRouterService;
		}
	};

	//Android的页面服务 ，TODO 编写配置状态栏颜色功能
	pageService: IDamPageService = {
		showToast: function (page: any, text: string, plantform?: PLANTFORM): boolean {
			if (text) {
				if (text.length > 5) {
					uni.showModal({
						title: '提示',
						content: text,
						showCancel: false
					});
				} else {
					uni.showToast({
						title: text
					});
				}
			}

			return true;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.ANDROID;
		},
		getSoftIndex: function (): number {
			return 40;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPageService;
		}
	};
	//Android的平台服务
	phoneAbilityService: IDamPhoneAbilityService = {
		registPlantformPageCallback: function (
			page: any,
			key: string,
			callback: IReceiveObserver,
			plantform?: PLANTFORM
		): boolean {
			// HyBirdServer.getInstance().addAppCallbackObserver(key, callback);
			return true;
		},
		unregistPlantformPageCallback: function (page: any, key: string, plantform?: PLANTFORM): boolean {
			HyBirdServer.getInstance().removeAppCallbackObserver(key);
			return true;
		},
		scanQrcode: function (page: any, plantform?: PLANTFORM): boolean {
			// try {
			// 	let message = HyBirdServer.getInstance().callApp({ action: Action.Scan_Qrcode, data: "" });
			// } catch (e) {
			// 	return false;
			// }
			return true;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.ANDROID;
		},
		getSoftIndex: function (): number {
			return 40;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPhoneAbilityService;
		}
	};
}
export enum Action {
	Scan_Qrcode = 90
}

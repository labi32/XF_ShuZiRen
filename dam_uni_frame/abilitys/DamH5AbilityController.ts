/**
 * DamH5PageController 主要是应用于H5页面中的处理
 */
import DamAbilityController from '@/dam_common/abilitys/DamAbilityController';
import { PLANTFORM, SERVICE_NAME } from '@/dam_common/abilitys/Constants';
import { IDamPageService } from '@/dam_common/abilitys/interface/IDamPageService';
import { IDamRouterService, IDamRouteOptions } from '@/dam_common/abilitys/interface/IDamRouterService';
import { IDamPhoneAbilityService } from '@/dam_common/abilitys/interface/IDamPhoneAbilityService';

import HyBirdServer, { IReceiveObserver } from '@/dam_common/hybird/HyBirdServer';
export default class DamH5AbilityController {
	registServices(controller: DamAbilityController) {
		controller.addServices(this.routerService, this.pageService, this.phoneAbilityService);
	}

	//H5的路由服务,h5跳转方式使用window的监听
	routerService: IDamRouterService = {
		getRouterParams: function (page: any, options: any): {} {
			let params = DamH5AbilityController._getPageParams(options.data);
			// DamH5AbilityController._getPageParams(options)
			return params;
		},
		//tip 如果有大数据量，可以使用uni.navigateTo 中的events
		navigateTo: function (options: IDamRouteOptions): boolean {
			let finalParams = '';
			if (options.data) {
				let params = DamH5AbilityController._createPageParams(options.data);
				if (params && params !== '') {
					finalParams = '?data=' + params;
				}
			}
			let finalEvents = options.events || {};
			if (options.isReload) {
				finalEvents.onRefresh = function (e) {
					options.reloadFunc(e);
				};
			}
			uni.navigateTo({
				url: options.path + finalParams,
				events: finalEvents
			});
			// // #ifdef VUE3
			// page.push({
			// 	path: path,
			// 	params: DamH5AbilityController._createPageParams(data)
			// });
			// // #endif
			// //  #ifndef VUE3
			// page.$router.push({
			// 	path: path,
			// 	params: DamH5AbilityController._createPageParams(data)
			// });
			// // #endif
			return true;
		},
		redirectTo: function (options: IDamRouteOptions): boolean {
			let finalParams = '';
			if (options.data) {
				let params = DamH5AbilityController._createPageParams(options.data);
				if (params && params !== '') {
					finalParams = '?data=' + params;
				}
			}
			let finalEvents = options.events || {};
			if (options.isReload) {
				finalEvents.onRefresh = function (e) {
					options.reloadFunc(e);
				};
			}
			uni.redirectTo({
				url: options.path + finalParams,
				events: finalEvents
			});
			// // #ifdef VUE3
			// page.replace({
			// 	path: path,
			// 	params: DamH5AbilityController._createPageParams(data)
			// });
			// // #endif
			// //  #ifndef VUE3
			// page.$router.replace({
			// 	path: path,
			// 	params: DamH5AbilityController._createPageParams(data)
			// });
			// // #endif
			return true;
		},
		reLaunch: function (options: IDamRouteOptions): boolean {
			let finalParams = '';
			if (options.data) {
				let params = DamH5AbilityController._createPageParams(options.data);
				if (params && params !== '') {
					finalParams = '?data=' + params;
				}
			}
			uni.reLaunch({
				url: options.path + finalParams
			});
			return false;
		},
		navigateback: function (options?: IDamRouteOptions): boolean {
			// // #ifdef VUE3
			// page.go(to ? to : -1);
			// // #endif
			// //  #ifndef VUE3
			// page.$router.go(to ? to : -1);
			// // #endif
			uni.navigateBack({});
			return true;
		},
		refresh: function (options: IDamRouteOptions): boolean {
			// #ifdef VUE3
			options.page.go(0);
			// #endif
			//  #ifndef VUE3
			options.page.$router.go(0);
			// #endif
			return true;
		},
		refreshBack: function (options: IDamRouteOptions): boolean {
			options?.page?.proxy?.getOpenerEventChannel().emit('onRefresh', options.data);
			return true;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.WINDOWS;
		},
		getSoftIndex: function (): number {
			return 90;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamRouterService;
		}
	};

	//H5的页面服务
	pageService: IDamPageService = {
		showToast: function (page: any, text: string, plantform?: PLANTFORM): boolean {
			return false;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.WINDOWS;
		},
		getSoftIndex: function (): number {
			return 90;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPageService;
		}
	};
	//H5的平台服务
	phoneAbilityService: IDamPhoneAbilityService = {
		registPlantformPageCallback: function (
			page: any,
			key: string,
			callback: IReceiveObserver,
			plantform?: PLANTFORM
		): boolean {
			HyBirdServer.getInstance().addAppCallbackObserver(key, callback);
			return true;
		},
		unregistPlantformPageCallback: function (page: any, key: string, plantform?: PLANTFORM): boolean {
			HyBirdServer.getInstance().removeAppCallbackObserver(key);
			return true;
		},
		scanQrcode: function (page: any, plantform?: PLANTFORM): boolean {
			throw new Error('Function not implemented.');
		},
		// swipeNFC: function (page : any, plantform ?: PLANTFORM) : boolean {
		// 	try {
		// 		let message = HyBirdServer.getInstance().callApp({ action: Action.Swipe_NFC, data: "" });
		// 		return message;
		// 	} catch (e) {
		// 	}
		// 	return false;
		// },
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.WINDOWS;
		},
		getSoftIndex: function (): number {
			return 90;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPhoneAbilityService;
		}
	};
	/**
	 * 拼接页面跳转的参数
	 */
	static _createPageParams(data?: any): string {
		let finalParams = '';
		if (data) {
			finalParams = encodeURIComponent(JSON.stringify(data));
		}
		return finalParams;
	}
	/**
	 * 获取页面跳转的参数
	 */
	static _getPageParams(data?: string): any {
		if (data) {
			let dataStr = decodeURIComponent(data);
			return JSON.parse(dataStr);
		}
		return null;
	}
	static getUrlParams(page: any) {
		var url = window.location.search;
		var params = new Object();
		if (url.indexOf('?') != -1) {
			var str = url.substr(1);
			var strs = str.split('&');
			for (var i = 0; i < strs.length; i++) {
				params[strs[i].split('=')[0]] = strs[i].split('=')[1];
			}
		}
		return params;
	}
}
export enum Action {
	// Scan_Qrcode = 90,
	INIT = 60,
	Swipe_NFC = 80
}

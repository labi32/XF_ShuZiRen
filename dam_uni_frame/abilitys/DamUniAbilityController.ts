/**
 * DamUniPageController 主要是应用于UNI页面中的处理
 */
import DamAbilityController from '@/dam_common/abilitys/DamAbilityController';
import { PLANTFORM, SERVICE_NAME } from '@/dam_common/abilitys/Constants';
import { IDamPageService } from '@/dam_common/abilitys/interface/IDamPageService';
import { IDamRouterService, IDamRouteOptions } from '@/dam_common/abilitys/interface/IDamRouterService';
import { IDamPhoneAbilityService } from '@/dam_common/abilitys/interface/IDamPhoneAbilityService';
import { IReceiveObserver } from '@/dam_common/hybird/HyBirdServer';

export default class DamUniAbilityController {
	registServices(controller: DamAbilityController) {
		controller.addServices(this.routerService, this.pageService, this.phoneAbilityService);
	}
	//Uni的路由服务,此服务在使用时，配合uni的钩子函数，进行路由鉴权
	routerService: IDamRouterService = {
		getRouterParams: function (page: any, options: any): {} {
			//TODO 要做好方案
			return undefined;
		},
		navigateTo: function (options: IDamRouteOptions): boolean {
			let finalEvents = options.events || {};
			if (options.isReload) {
				finalEvents.onRefresh = function (e) {
					options.reloadFunc(e);
				};
			}
			uni.navigateTo({
				url: options.path + DamUniAbilityController._createPageParams(options.data),
				events: finalEvents,
				needAuth: options.needAuth
			});
			return true;
		},
		redirectTo: function (options: IDamRouteOptions): boolean {
			let finalEvents = options.events || {};
			if (options.isReload) {
				finalEvents.onRefresh = function (e) {
					options.reloadFunc(e);
				};
			}
			uni.redirectTo({
				url: options.path + DamUniAbilityController._createPageParams(),
				events: finalEvents,
				needAuth: options.needAuth
			});
			return true;
		},
		reLaunch: function (options: IDamRouteOptions): boolean {
			uni.reLaunch({
				url: options.path + DamUniAbilityController._createPageParams(),
				needAuth: options.needAuth
			});
			return false;
		},
		navigateback: function (options?: IDamRouteOptions): boolean {
			uni.navigateBack({
				needAuth: options ? options.needAuth : undefined
			});
			return true;
		},
		refresh: function (options: IDamRouteOptions): boolean {
			//TODO 此处暂时没想到怎么处理
			return false;
		},
		refreshBack: function (options: IDamRouteOptions): boolean {
			options?.page?.proxy?.getOpenerEventChannel().emit('onRefresh', options.data);
			return true;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.UNI;
		},
		getSoftIndex: function (): number {
			return 80;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamRouterService;
		}
	};

	//Uni的页面服务
	pageService: IDamPageService = {
		showToast: function (page: any, text: string, plantform?: PLANTFORM): boolean {
			if (text) {
				if (text.length > 5) {
					uni.showModal({
						title: '提示',
						content: text,
						showCancel: false
					});
				} else {
					uni.showToast({
						title: text
					});
				}
			}

			return true;
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.UNI;
		},
		getSoftIndex: function (): number {
			return 80;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPageService;
		}
	};
	//Uni的平台服务
	phoneAbilityService: IDamPhoneAbilityService = {
		registPlantformPageCallback: function (
			page: any,
			key: string,
			callback: IReceiveObserver,
			plantform?: PLANTFORM
		): boolean {
			throw new Error('Function not implemented.');
		},
		unregistPlantformPageCallback: function (page: any, key: string, plantform?: PLANTFORM): boolean {
			throw new Error('Function not implemented.');
		},
		scanQrcode: function (page: any, plantform?: PLANTFORM): boolean {
			throw new Error('Function not implemented.');
		},
		getPlantform: function (): PLANTFORM {
			return PLANTFORM.UNI;
		},
		getSoftIndex: function (): number {
			return 80;
		},
		getServiceName: function (): SERVICE_NAME {
			return SERVICE_NAME.DamPhoneAbilityService;
		}
	};
	/**
	 * 拼接页面跳转的参数
	 */
	static _createPageParams(data?: any): string {
		let finalParams = '';
		if (data && data.length > 0) {
			finalParams = '?' + 'data=' + encodeURIComponent(data);
		}
		return finalParams;
	}
	/**
	 * 获取页面跳转的参数
	 */
	static _getPageParams(data?: string): any {
		if (data) {
			let dataStr = decodeURIComponent(data);
			if (dataStr.startsWith('?data=')) {
				dataStr = dataStr.replace('?data=', '');
			}
			return dataStr;
		}
		return null;
	}
}

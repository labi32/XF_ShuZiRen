/**
 * 全局方法的混入，需要在main.js中,VUE 3 中不采用混入
 *
 */

import LogUtil from "@/dam_common/utils/LogUtil"
import DamBridge from '@/dam_common/abilitys/DamBridge';
import Constants from '@/dam_uni_frame/utils/Constants';
import {
	IReceiveObserver,
	Message
} from "@/dam_common/hybird/HyBirdServer";
import {
	ref,
	onMounted,
	onUnmounted
} from 'vue'


export default {
	data() {
		return {

		}
	},
	methods: {
		/**
		 * 打印日志
		 * @param {Object} param
		 */
		printLog(param) {
			if (process.env.NODE_ENV === 'development') {
				LogUtil.printInfo(param);
			}
		},
		isTextEmpty(txt) {
			return !txt || txt == '';
		},
		// getMainPageService() {
		// 	return DamBridge.getDamPageService(Constants.App_Main);
		// },
		// getMainAbilityService() {
		// 	return DamBridge.getDamAbilityService(Constants.App_Main);
		// },
		// #ifndef VUE3
		hookAppCallback() {
			this.getMainAbilityService().scanQrcode(this);
			let that = this
			let callback : IReceiveObserver = {
				observe(data) : boolean {
					that.getMainPageService().showToast(that, "aaqqqqq", PLANTFORM.ALL);
					return true;
				}
			}
			this.getMainAbilityService().registPlantformPageCallback(this, "test", callback);
		}
		// #endif
	},

}
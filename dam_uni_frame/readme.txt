dam_uni_frame主要是为了整体框架的搭建，以及逻辑处理。TIPS：使用dam_common中的基础方法来搭建


注意：
1、组件注册：
	"easycom": {
		"autoscan": true,
		"custom": {
			"^dam-(.*)": "@/dam_uni_frame/components/view/dam-$1.vue" // 匹配dam_uni_frame中components目录内的vue文件，自动挂载到全局组件
		}
	},
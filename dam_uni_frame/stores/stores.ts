// 定义仓库有两种定义方式

// 01 选项options方式
// export const storeInstance = defineStore('counter', {
// 			// 定义状态
// 			state: () => ({
// 				orientation: null
// 			}),
// 			// 计算数据
// 			getters: {
// 				isPortrait: (state) => {
// 					if (state)
// 						//竖屏
// 						return orientation.type.includes('portrait'));
// 				else return false;
// 			}
// 		}
// 	},
// 	// 动作支持异步
// 	actions: {
// 		setOrientation(orientation) {
// 			this.orientation = orientation;
// 		}
// 	}
// })

//02

// 导入定义仓库的方法
import {
	defineStore
} from 'pinia';

// 导入响应式和计算
import {
	ref
} from 'vue'
import DamStore from '@/dam_common/respository/data/dam_store';
const useBusinessStore = defineStore("business", () => {
	const userId = ref(''); //用户ID
	const openId = ref(''); //openID
	const token = ref(''); //token

	/**
	 * 获取用户ID
	 */
	const getUserId = () => {
		return userId.value;
	}

	/**
	 * 获取OpenID
	 */
	const getOpenId = () => {
		return openId.value;
	}

	/**
	 * 获取token
	 */
	const getToken = () => {
		return token.value;
	}
	/**
	 * 设置用户信息
	 */
	const setUserInfo = userInfo => {
		if (userInfo) {
			userId.value = userInfo.UserId;
			openId.value = userInfo.OpenId;
			token.value = userInfo.Token;
		} else {
			userId.value = '';
			openId.value = '';
			token.value = '';
		}
	};

	return {
		getUserId,
		getOpenId,
		getToken,
		setUserInfo
	}

})

const usePageStore = defineStore("page", () => {
	// const screenWidthReference = 1080; //屏幕宽度基准
	// const screenHeightReference = 1920; //屏幕宽度基准
	//方向
	const orientation = ref(null);
	const isPortrait = ref(true);

	//页面尺寸相关
	const screenRes = ref(); //屏幕信息
	const toolbarH = ref(44); //标题栏的高度
	const statusH = ref(0); //状态栏的高度
	const safeAreaTopH = ref(0); //安全区域的顶部高度
	const safeAreaBottomH = ref(0); //安全区域的底部高度
	const windowH = ref(0); //窗口高度
	const windowW = ref(0); //窗口宽度
	// const fontSizeScale = ref(1); //文字大小缩放
	const contentHWithoutToolbar = ref(0); //除去状态栏及标题栏的高度

	//get
	//获取标题栏实际高度，因为是沉浸式，所以要加上状态栏的高度
	const getToolbarHeight = () => {
		return (toolbarH.value + safeAreaTopH.value) + 'px';
	};
	const getStatusHeight = () => {
		return statusH.value + 'px';
	};
	const getSafeAreaTopHeight = () => {
		return safeAreaTopH.value + 'px';
	};
	const getSafeAreaBottomHeight = () => {
		return safeAreaBottomH.value + 'px';
	};
	const getWindowHeight = () => {
		return windowH.value + 'px';
	};
	const getWindowWidth = () => {
		return windowW.value + 'px';
	};
	const getContentHWithoutToolbar = () => {
		return contentHWithoutToolbar.value + 'px';
	};

	const setOrientation = ori => {
		orientation.value = ori;
		isPortrait.value = isScreenPortrait();
		if (screenRes.value) {
			setWindoHeight(screenRes.value.windowHeight);
			setWindoWidth(screenRes.value.windowWidth);
			// if (isPortrait.value) {
			// 	setWindoHeight(screenRes.value.windowHeight);
			// 	setWindoWidth(screenRes.value.windowWidth);
			// } else {
			// 	setWindoHeight(screenRes.value.windowWidth);
			// 	setWindoWidth(screenRes.value.windowHeight);
			// }
		}
	};
	const setScreenRes = (res) => {
		screenRes.value = res;
		safeAreaTopH.value = res.safeAreaInsets.top;
		safeAreaBottomH.value = res.safeAreaInsets.bottom;
	};
	const setSafeAreaTopHeight = (height) => {
		return safeAreaTopH.value = height;
	};
	const setSafeAreaBottomHeight = (height) => {
		return safeAreaBottomH.value = height;
	};
	const isScreenPortrait = () => {
		if (orientation) {
			if (orientation.value) {
				//竖屏
				return orientation.value.includes('portrait');
			}
		}
		return true;

	};
	const setToolbarHeight = height => {
		toolbarH.value = height;
		caculateContentHeight();
	};
	const setStatusHeight = height => {
		statusH.value = height;
		caculateContentHeight();
	};
	const setWindoHeight = height => {
		windowH.value = height;
		caculateContentHeight();
	};
	const setWindoWidth = width => {
		windowW.value = width;
		// if (isPortrait.value) {
		// 	fontSizeScale.value = windowW.value / screenWidthReference;
		// } else {
		// 	fontSizeScale.value = windowH.value / screenWidthReference;
		// }
	};

	function caculateContentHeight() {
		contentHWithoutToolbar.value = windowH.value - toolbarH.value - safeAreaTopH.value;
	}
	// 导入
	return {
		//变量
		// fontSizeScale,
		orientation,
		isPortrait,
		toolbarH,
		statusH,
		safeAreaTopH,
		safeAreaBottomH,
		windowH,
		windowW,
		contentHWithoutToolbar,
		// 方法
		setScreenRes,
		setSafeAreaTopHeight,
		setSafeAreaBottomHeight,
		setOrientation,
		isScreenPortrait,
		setToolbarHeight,
		setStatusHeight,
		setWindoHeight,
		setWindoWidth,
		//get
		getToolbarHeight,
		getStatusHeight,
		getSafeAreaTopHeight,
		getSafeAreaBottomHeight,
		getWindowHeight,
		getWindowWidth,
		getContentHWithoutToolbar
	}
})

export {
	useBusinessStore,
	usePageStore
};
1、dam_theme.scss 与 dam_theme_handle.scss废弃

2、dam_theme中命名不要用_，而是用下划线_ 
描述：

"color_bg": "$dam_color_white",
"color_bg_half": "$dam_color_trans_half_light",
// toolbar_color
//半透明的按钮
"color_btn_half": "$dam_color_trans_half_dark",
//按钮中文字的颜色_白色
"color_btn_font_white": "$dam_color_font_white",
//大标题类文字颜色
"color_font_title_big": "$dam_color_blue",
//提示类文字颜色
"color_font_tip": "$dam_color_gray_2",
//普通标题类文字颜色，如item
"color_font_title_normal": "$dam_color_font_dark",
//录入框，文字的颜色
"color_input_font": "$dam_color_font_dark",
//录入框，文字的颜色
"color_input_placeholder": "$dam_color_gray_2",
//录入框，border的颜色
"color_input_border": "$dam_color_gray",
//录入框的focus状态下，border的颜色，
"color_input_border_hover": "$dam_color_blue_soft",
//选中的图标颜色
"color_icon_check": "$dam_color_blue",
//按钮背景的颜色,主题色
"color_btn_bg_theme": "$dam_color_blue",
//按钮文字的颜色,白色
"color_btn_font_theme": "$dam_color_font_white",
//按钮按下时背景的颜色,主题色
"color_btn_bg_theme_soft": "$dam_color_blue_soft",
//按钮文字大小 _ 小的tip
"size_btn_font_tip": "$dam_size_font_small"
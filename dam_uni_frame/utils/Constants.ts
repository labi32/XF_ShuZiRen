export default {
	//主App,bridge挂载的Key
	App_Main: "main",
	//H5蒙版App
	App_H5: "h5page",
	//本地信息key，用户信息
	KEY_STORAGE_USER: "UserInfo"
}
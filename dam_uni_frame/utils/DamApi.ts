/**
 * 公用方法池
 */
import LogUtil from '@/dam_common/utils/LogUtil';
import DamBridge from '@/dam_common/abilitys/DamBridge';
import Constants from '@/dam_uni_frame/utils/Constants';
import { IReceiveObserver } from '@/dam_common/hybird/HyBirdServer';
import { PLANTFORM } from '@/dam_common/abilitys/Constants';
import DamAbilityController, { Executor } from '@/dam_common/abilitys/DamAbilityController';
import HttpProcessor from '@/dam_common/respository/http/dam-http';
import { KEY as AppHttoKey } from '@/respository/http/HttpBuilder';
// import ThemeColorUtil from "./ThemeColorUtil";
import DamStore from '@/dam_common/respository/data/dam_store';
import { getConfig } from '@/dam_uni_frame/utils/DebugConfig';
/**
 * 打印日志
 * @param  param
 */
const printLog = function (...param) {
	LogUtil.printInfo(param);
};
/**
 * 文字是否是空
 */
const isTextEmpty = function (txt) {
	return !txt || txt == '';
};
/**
 * 获取App的Http实例
 */
const getAppHttp = function (): HttpProcessor {
	return getAbilityController().getHttpProcessor(AppHttoKey);
};
/**
 * 获取能力控制器
 */
const getAbilityController = function (): DamAbilityController {
	let controller = DamBridge.getDamAbilityController(Constants.App_Main);
	let actualController = controller as DamAbilityController;
	return actualController;
};
/**
 * 获取能力执行器
 */
const getAbilityExector = function (): Executor {
	return getAbilityController().executor;
};
// /**
//  * 获取主题数据
//  * 此方法赋值给页面中的变量，如果是设置主题页面，需要在设置页面使用ref来更换theme值
//  */
// const getTheme = function () {
// 	// let app = DamBridge.getApp(Constants.App_Main);
// 	return ThemeColorUtil.getCurrentThemeData()
// };

const hookAppCallback = function (page) {
	let callback: IReceiveObserver = {
		observe(data): boolean {
			// getAbilityExector().showToast(page, "aaqqqqq", PLANTFORM.ALL)
			return true;
		}
	};
	getAbilityExector().registPlantformPageCallback(this, 'test', callback);
};

const logout = function (page, busStore) {
	DamStore.remove(Constants.KEY_STORAGE_USER);
	if (busStore) {
		busStore.setUserInfo(null);
	}
	getAbilityExector().reLaunch({ path: '/subs/jz_driver/pages/login/index' });
};

const getAppSettings = function () {
	return getConfig();
};
export {
	printLog,
	isTextEmpty,
	getAppHttp,
	getAbilityController,
	getAbilityExector,
	hookAppCallback,
	logout,
	getAppSettings
};

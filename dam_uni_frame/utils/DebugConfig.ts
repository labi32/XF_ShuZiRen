import Settings from "@/Settings.json";

//是否是debug模式
export function isDebug() {
	return Settings.isDebug;
}

//获取配置项
export function getConfig() {
	return isDebug() ? Settings.debug : Settings.release;
}
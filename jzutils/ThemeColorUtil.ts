/**
 * 设置主题，取消了vue2的支持，使用vue3，加载json文件的方式加载主题色
 * Tips：scss中使用v-bind绑定颜色值
 */
import {
	defineStore
} from 'pinia';
import {
	ref
} from 'vue';
import DamStore from "@/dam_common/respository/data/dam_store";
import ObjectUtil from "@/dam_common/utils/ObjectUtil"
import ThemeData from "@/static/css/dam_theme_color.json"

const StoreKey = "theme";

const useThemeStore = defineStore(StoreKey, () => {
	const themeName = ref(''); //当前主题
	const themeData = ref([]); //主题数据

	const setThemeName = (_themeName) => {
		themeName.value = _themeName;
	};
	const setThemeData = (_themeData) => {
		themeData.value = _themeData;
	};
	const getThemeName = () => {
		return themeName.value;
	};
	const getThemeData = () => {
		return themeData.value;
	};
	return {
		themeName,
		themeData,
		setThemeName,
		setThemeData,
		getThemeName,
		getThemeData,
	}
});

/**
 * 初始化主题
 */
function initTheme() {
	let themeJson = ThemeData;
	let value = DamStore.get(StoreKey);
	let theme = "default";
	if (value && value !== '') {
		theme = value;
	}
	changeThemeWithData(theme, themeJson);
}

/**
 * 获取当前的主题
 */
function getCurrentThemeName() {
	let themeStore = useThemeStore();
	return themeStore.getThemeName();
}

/**
 * 获取当前的主题
 */
function getCurrentThemeData() {
	let themeStore = useThemeStore();
	return themeStore.getThemeData();
}

/**
 * 改变主题
 * @param {String} theme
 */
function changeTheme(theme) {
	return changeThemeWithData(theme, ThemeData);
}
/**
 * 改变主题
 * @param {String} theme
 * @param themeJson 样式json
 */
function changeThemeWithData(theme, themeJson = []) {
	let themeStore = useThemeStore();
	let _themeJson = themeJson
	if (!themeJson) {
		_themeJson = ThemeData;
		return;
	}
	themeStore.setThemeName(theme);
	themeStore.setThemeData(_themeJson[theme]);
	return DamStore.save(StoreKey, theme)
}

// /**
//  * 注册样式变化监听
//  * @param {App} app
//  * @param {Function} func
//  */
// function registThemeChange(app, func) {
// 	ObjectUtil.watchDataChanged(app.globalData, "page", function(key, value) {
// 		func(value.theme)
// 	})
// }
export default {
	useThemeStore,
	initTheme,
	getCurrentThemeName,
	getCurrentThemeData,
	changeTheme
}
/**
 * 设置尺寸，取消了vue2的支持，使用vue3，加载json文件的方式加载尺寸大小
 * Tips：scss中使用v-bind绑定值
 */
import {
	defineStore
} from 'pinia';
import {
	ref
} from 'vue';
import DamStore from "@/dam_common/respository/data/dam_store";
import ObjectUtil from "@/dam_common/utils/ObjectUtil"
import ThemeSizesData from "@/dam_common/styles/dam_theme_sizes.json"

const StoreKey = "sizes";

const useThemeSizesStore = defineStore(StoreKey, () => {
	const themeSizesName = ref(''); //当前尺寸
	const themeSizesData = ref([]); //尺寸数据

	const setThemeSizesName = (_themeSizesName) => {
		themeSizesName.value = _themeSizesName;
	};
	const setThemeSizesData = (_themeSizesData) => {
		themeSizesData.value = _themeSizesData;
	};
	const getThemeSizesName = () => {
		return themeSizesName.value;
	};
	const getThemeSizesData = () => {
		return themeSizesData.value;
	};
	return {
		themeSizesName,
		themeSizesData,
		setThemeSizesName,
		setThemeSizesData,
		getThemeSizesName,
		getThemeSizesData,
	}
});

/**
 * 初始化主题
 */
function initThemeSizes() {
	let themeSizesJson = ThemeSizesData;
	let value = DamStore.get(StoreKey);
	let themeSizes = "default";
	if (value && value !== '') {
		themeSizes = value;
	}
	changeThemeSizesWithData(themeSizes, themeSizesJson);
}

/**
 * 获取当前的主题
 */
function getCurrentThemeSizesName() {
	let themeSizesStore = useThemeSizesStore();
	return themeSizesStore.getThemeSizesName();
}

/**
 * 获取当前的主题
 */
function getCurrentThemeSizesData() {
	let themeSizesStore = useThemeSizesStore();
	return themeSizesStore.getThemeSizesData();
}

/**
 * 改变主题
 * @param {String} themeSizes
 */
function changeThemeSizes(themeSizes) {
	return changeThemeSizesWithData(themeSizes, ThemeSizesData);
}
/**
 * 改变主题
 * @param {String} themeSizes
 * @param themeSizesJson 样式json
 */
function changeThemeSizesWithData(themeSizes, themeSizesJson = []) {
	let themeSizesStore = useThemeSizesStore();
	let _themeSizesJson = themeSizesJson
	if (!themeSizesJson) {
		_themeSizesJson = ThemeData;
		return;
	}
	themeSizesStore.setThemeSizesName(themeSizesSizes);
	themeSizesStore.setThemeSizesData(_themeSizesJson[themeSizes]);
	return DamStore.save(StoreKey, themeSizes)
}

// /**
//  * 注册样式变化监听
//  * @param {App} app
//  * @param {Function} func
//  */
// function registThemeSizesChange(app, func) {
// 	ObjectUtil.watchDataChanged(app.globalData, "page", function(key, value) {
// 		func(value.themeSizes)
// 	})
// }
export default {
	useThemeSizesStore,
	initThemeSizes,
	getCurrentThemeSizesName,
	getCurrentThemeSizesData,
	changeThemeSizes
}
import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import { createPinia } from 'pinia'
import DamScrollPage from '@/dam_uni_frame/components/dam-scroll-page/dam-scroll-page.vue'
import DamPage from '@/dam_uni_frame/components/dam-page/dam-page.vue'

export function createApp() {
	const app = createSSRApp(App)
	const pinia = createPinia()
	
	app.use(pinia)
	app.component('dam-scroll-page', DamScrollPage)
	app.component('dam-page', DamPage)
	
	return {
		app
	}
}
// #endif
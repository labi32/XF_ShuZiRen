# 数字人实验室预约系统

## 项目概述

数字人实验室预约系统是一个面向高校数字人相关实验室的管理与预约平台，提供实验室和设备的查询、预约、管理等功能。系统集成了数字人交互和智能对话服务，为用户提供沉浸式的交互体验和智能化的信息查询服务。

## 系统特色

- **数字人交互**：集成数字人技术，提供直观的视觉交互界面
- **智能对话辅助**：基于大语言模型的实验室智能助手，帮助用户快速获取信息
- **完整预约流程**：提供从查询、筛选到预约确认的完整实验室预约流程
- **多模型支持**：支持多种AI模型切换，适应不同场景需求
- **流式响应集成**：数字人可实时读出AI模型的流式响应，创造自然的交互体验

## 主要功能

### 1. 账户管理
- 用户注册与登录
- 个人信息查看与管理
- 身份验证与权限控制

### 2. 实验室管理
- 实验室列表查询与筛选
- 实验室详情查看
- 实验室设备管理
- 实验室状态监控

### 3. 预约系统
- 实验室可用状态查询
- 设备预约时段选择
- 预约确认与记录管理
- 预约历史查询
- 预约状态跟踪

### 4. 智能交互
- 数字人形象展示与交互
- 智能对话咨询服务
- 智能模型与数字人联动
- 多轮对话历史记录
- 实时语音反馈

## 技术架构

### 前端技术栈
- **开发框架**：uni-app
- **UI组件**：uni-ui、自定义组件
- **状态管理**：Pinia
- **UI框架**：uv-ui

### 后端技术
- **云服务**：腾讯云开发
- **数据库**：云数据库
- **API接口**：RESTful API

### 代码结构
- **api/**
  - `function.js`: 核心功能函数实现，包含预约管理、实验室查询等功能
  - `functionDefinitions.js`: AI模型工具函数定义，提供AI调用的函数接口
  - `openaiService.js`: OpenAI服务封装，处理AI模型请求与响应
  - `appwx.js`: 云开发数据库操作封装
- **pages/**
  - `chat/`: 智能对话页面及相关组件
    - `comps/`: 对话页面组件（消息列表、输入框等）
    - `chat.vue`: 主对话界面
  - `avatar/`: 数字人交互页面
    - `avatar.vue`: 数字人渲染与控制
  - `lab/`: 实验室相关页面
    - `lab-list.vue`: 实验室列表
    - `lab-detail.vue`: 实验室详情
  - `reservation/`: 预约相关页面
    - `reservation-form.vue`: 预约表单
    - `reservation-list.vue`: 预约记录列表
  - `user/`: 用户相关页面
    - `login.vue`: 登录页面
    - `register.vue`: 注册页面
    - `profile.vue`: 个人信息页面
- **components/**: 可复用组件
  - `comp-loading/`: 加载组件
  - `comp-empty/`: 空状态组件
  - `comp-avatar/`: 数字人相关组件
  - `comp-dialog/`: 对话框组件
- **utils/**: 工具函数
  - `request.js`: 网络请求封装
  - `date.js`: 日期处理工具
  - `storage.js`: 本地存储工具
  - `validator.js`: 表单验证工具
- **static/**: 静态资源
  - `css/`: 样式文件
    - `iconfont.css`: 图标字体样式
    - `common.css`: 公共样式
  - `images/`: 图片资源
  - `fonts/`: 字体文件
- **stores/**: 状态管理
  - `user.js`: 用户状态管理
  - `chat.js`: 对话状态管理
  - `reservation.js`: 预约状态管理
- **config/**: 配置文件
  - `api.js`: API接口配置
  - `models.js`: AI模型配置
  - `theme.js`: 主题配置

### 集成服务
- **讯飞数字人SDK**：提供数字人渲染与交互能力
  - 实时渲染与动画
  - 文本驱动与标签识别
  - 流式文本处理与追加模式
- **AI模型接口**：提供智能对话服务，支持多种模型
  - 基础模型 (阿里千问)
  - 深度思考模型 (Deepseek)
  - 高级对话模型 (豆包)

## 页面功能介绍

### 登录注册页面
- 学号/工号登录
- 新用户注册
- 身份验证
- 密码找回

### 首页
- 实验室状态概览
- 统计数据展示
- 快速导航入口
- AI模型集成
  - 多模型选择切换
  - 实时流式响应
  - 智能对话能力
  - 标点智能分段发送

### 数字人页面
- 数字人渲染与展示
- 文本驱动数字人交互
- 播放控制查看
- AI模型集成
  - 多模型选择切换
  - 实时流式响应
  - 智能对话能力
  - 标点智能分段发送

### 实验室列表页面
- 实验室分类展示
- 状态筛选与搜索
- 详情快速查看
- 实时状态更新

### 实验室详情页面
- 实验室完整信息
- 设备列表与状态
- 预约入口查看
- 使用说明展示

### 预约页面
- 日期与时段选择
- 设备筛选
- 预约信息确认
- 预约规则提示

### 预约历史页面
- 用户预约记录查询
- 预约状态跟踪
- 预约详情查看
- 预约记录导出

### 智能对话页面
- 多轮对话界面
- 模型切换
- 会话历史管理
- 实时语音反馈

## 数据库结构

### 1. 用户信息集合 (grxx)
存储用户基本信息，包括：
- `_id`: 用户ID（系统自动生成）
- `xh`: 学号/工号
- `mm`: 密码
- `name`: 姓名
- `nj`: 年级
- `xy`: 学院
- `bj`: 班级
- `phone`: 电话号码
- `typetype`: 用户类型（本科生、实验室管理员、超级管理员）
- `glsysName`: 管理的实验室名称（仅管理员）

### 2. 实验室列表集合 (syslb)
存储实验室信息，包括：
- `_id`: 实验室ID（系统自动生成，格式：lab001-lab010）
- `ms`: 实验室描述
- `name`: 实验室名称
- `sbName`: 设备名称数组
- `address`: 地址
- `isShow`: 是否显示
- `sbID`: 设备ID数组
- `sbsl`: 设备数量
- `updateBy`: 更新人ID
- `updatedAt`: 更新时间戳
- `ssxy`: 所属学院
- `status`: 状态（1-可预约，2-已约满，3-未开放，4-维护中，5-建设中）

### 3. 设备列表集合 (sblb)
存储设备信息，包括：
- `_id`: 设备ID（系统自动生成，格式：eq001-eq030）
- `createdAt`: 创建时间戳
- `updatedAt`: 更新时间戳
- `owner`: 设备负责人ID
- `createBy`: 创建人ID
- `updateBy`: 更新人ID
- `_departmentList`: 部门列表
- `name`: 设备名称
- `detail`: 设备详情描述
- `sysID`: 所属实验室ID
- `sysName`: 所属实验室名称
- `xyName`: 所属学院名称
- `status`: 状态（可预约、已被约、暂停开放、建设中、故障中、维护中、未开放）

### 4. 预约记录集合 (yyjl)
存储预约记录，包括：
- `_id`: 预约记录ID（系统自动生成）
- `personName`: 预约人姓名
- `personID`: 预约人ID
- `stauts`: 预约状态（已预约、pending、approved、rejected、cancelled、completed）
- `time`: 预约时间段数组（1-8:00-10:00，2-10:00-12:00，3-14:00-16:00，4-16:00-18:00）
- `date`: 预约日期
- `sysName`: 实验室名称
- `sysID`: 实验室ID
- `sbName`: 设备名称
- `sbID`: 设备ID

## 状态编码说明

### 实验室状态
- `1`: 可预约
- `2`: 已约满
- `3`: 未开放
- `4`: 维护中
- `5`: 建设中

### 设备状态
- `可预约`: 设备当前可以预约
- `已被约`: 设备已被预约
- `暂停开放`: 设备暂时不开放预约
- `建设中`: 设备正在建设阶段
- `故障中`: 设备出现故障
- `维护中`: 设备正在维护
- `未开放`: 设备未开放使用

### 预约状态
- `已预约`: 已完成预约
- `pending`: 待审核
- `approved`: 已通过
- `rejected`: 已拒绝
- `cancelled`: 已取消
- `completed`: 已完成

## 时间段编码说明

预约系统中的时间段使用数字编码表示：
- `1`: 8:00-10:00
- `2`: 10:00-12:00
- `3`: 14:00-16:00
- `4`: 16:00-18:00

## 开发与部署

### 环境要求
- Node.js 12+
- HBuilderX 3.0+
- 微信开发者工具

### 本地开发
1. 克隆代码库
2. 安装依赖：`npm install`
3. 在HBuilderX中导入项目
4. 配置云开发环境
5. 运行项目

### 生产部署
1. 在HBuilderX中构建
2. 选择目标平台（小程序、H5等）
3. 上传部署

## 使用说明

### 普通用户
1. 注册/登录系统
2. 浏览实验室列表，查看可用实验室
3. 选择需要预约的实验室，进入详情页
4. 选择设备、日期和时间段
5. 确认预约信息，提交预约
6. 在预约历史中查看预约状态
7. 体验数字人智能交互功能

### 管理员用户
1. 使用管理员账号登录
2. 管理实验室和设备信息
3. 审核用户预约申请
4. 更新实验室和设备状态

## 数字人与AI模型集成

### 工作流程
1. **启动数字人**：初始化SDK并连接讯飞数字人服务
2. **用户提问**：用户在输入框中输入问题
3. **选择交互模式**：
   - 直接发送：文本直接传递给数字人
   - 发送到模型：文本先发送给AI模型处理
4. **模型处理与响应**：
   - 系统将问题发送到选定的AI模型
   - 接收模型的流式响应
   - 展示响应内容在界面上
5. **智能分段处理**：
   - 系统按标点符号智能分段处理模型响应
   - 确保每段文本长度适中，避免卡顿
6. **流式传输到数字人**：
   - 使用append模式将分段文本传给数字人
   - 数字人实时播报内容，创造连贯体验
7. **多轮工具调用**：
   - 模型可自主决定是否调用工具获取数据
   - 支持连续多轮工具调用和中间推理
   - 基于连续工具结果形成最终回答
   - 数字人实时反馈处理进展
8. **自动资源管理**：
   - 页面关闭时自动停止数字人实例
   - 释放相关资源，避免内存泄漏

### 数字人SDK关键参数设置

#### writeText方法参数
数字人SDK提供了`writeText()`方法用于发送文本内容，该方法支持以下重要参数：

1. **追加模式（append）**
   - 设置为`true`：新文本将追加到当前正在播报的内容后
   - 设置为`false`（默认）：新文本将替换当前内容，从头开始播报

2. **交互模式（interactive_mode）**
   - 设置为`1`：打断模式，中断当前播报，立即播报新内容
   - 设置为`0`：不打断模式，将内容追加到当前播报队列末尾

3. **使用示例**
   ```javascript
   // 第一次发送，使用打断模式
   await avatarPlatformRef.writeText("第一段文本", { 
     interactive_mode: 1 
   });
   
   // 后续发送，使用追加且不打断模式
   await avatarPlatformRef.writeText("第二段文本", { 
     append: true, 
     interactive_mode: 0 
   });
   ```

#### 流式发送最佳实践
为确保数字人语音输出流畅自然，需注意以下事项：

1. **文本分段**：以标点符号为界，将长文本分成多个自然段落
2. **段落长度**：每段不宜过短（至少10-15个字符），避免播报卡顿
3. **首次调用**：使用打断模式（interactive_mode: 1）
4. **后续调用**：使用追加且不打断模式（append: true, interactive_mode: 0）
5. **延时控制**：在连续发送片段之间添加适当延时（50-100ms）
6. **空文本处理**：避免发送空文本或只有空格的文本

### 特色功能
1. **多模型支持**：支持三种不同的模型，满足不同场景需求
   - 基础模型：日常问答，速度快
   - 深度思考：复杂问题分析
   - 豆包专业版：专业领域知识
2. **智能分段处理**：基于标点符号的智能分段，确保数字人语音流畅
3. **流式响应集成**：模型的流式响应实时传递给数字人，减少等待时间
4. **连续工具调用**：模型可以根据需要多次调用工具，实现复杂推理
   - 支持工具间的上下文传递与结果关联
   - 在工具调用过程中进行推理和分析
   - 模型主动决定何时需要调用工具获取数据
   - 反馈工具调用状态，提高用户体验
5. **资源自动管理**：页面生命周期结束时自动释放资源

### 使用指南

#### 数字人交互
1. 点击"一键启动"按钮初始化数字人
2. 选择交互模式：
   - **直接交互**：在输入框中输入文本，点击"直接发送"按钮
   - **智能交互**：在输入框中输入问题，选择合适的模型，点击"发送到模型"按钮
3. 观察数字人响应和模型回复
4. 使用完毕后点击"一键停止"按钮释放资源

#### 模型选择
- **基础模型**：适用于简单问答，响应速度快
- **深度思考**：适用于需要推理和分析的复杂问题
- **豆包专业版**：适用于专业领域知识咨询

#### 注意事项
1. 确保网络连接稳定，以获得最佳的流式响应体验
2. 长时间不使用时，建议手动停止数字人以节省资源
3. 如遇播放受限，点击"恢复播放"按钮
4. 数字人播报过程中可能会出现轻微延迟，这是正常现象

## 新增功能：悬浮球组件

在应用主界面添加了一个可拖动的悬浮球组件，提供以下功能：

1. **悬浮显示**：默认显示在屏幕右下角
2. **拖拽移动**：用户可以自由拖动悬浮球到屏幕任意位置
3. **自动吸附**：释放后自动吸附到屏幕左侧或右侧
4. **点击跳转**：点击悬浮球会跳转到头像/个人中心页面

### 使用方法

1. 在页面中引入组件：

```vue
<template>
  <view>
    <!-- 其他内容 -->
    <float-ball imgSrc="/static/img/float-ball.png" :size="60"></float-ball>
  </view>
</template>

<script>
import FloatBall from '@/components/float-ball/float-ball.vue';

export default {
  components: {
    FloatBall
  },
  // ...
}
</script>
```

### 组件参数

| 参数 | 类型 | 默认值 | 说明 |
|------|------|--------|------|
| imgSrc | String | '/static/img/float-ball.png' | 悬浮球图片路径 |
| size | Number | 50 | 悬浮球大小（单位：px） |
| initialPosition | Object | { x: 'right', y: 'bottom', offsetX: 20, offsetY: 100 } | 初始位置配置 |

### 初始位置参数说明

- **x**: 水平位置，可选值：'left'、'right' 或具体数值
- **y**: 垂直位置，可选值：'top'、'bottom' 或具体数值
- **offsetX**: 水平偏移量（单位：px）
- **offsetY**: 垂直偏移量（单位：px）

### 注意事项

1. 请确保图片资源路径正确
2. 悬浮球使用固定定位，会覆盖在页面内容上方
3. 如需修改点击跳转页面，请修改组件内的 `handleClick` 方法

## 许可信息

© 2025 数字人实验室预约系统，保留所有权利

## 更新日志

### 2024-03-21
- 重构了AI模型工具函数结构
  - 将 `functionDefinitions` 从 `function.js` 分离到独立文件
  - 优化了代码组织，提高可维护性
  - 更新了所有相关文件的导入路径
- 改进了代码文档
  - 添加了详细的函数注释
  - 完善了错误处理说明
  - 更新了 README 文档结构说明












{
    "compilerOptions": {    
        //所有@引入的文件，都需要在这里配置
        "paths": {
            "@/*": ["./src/*"],
            //ts文件需单独配置
            "@/*.ts": ["./src/*.ts"],
            //这里是嵌套了一层引入的ts
            "@/utils/*.ts": ["./src/utils/*.ts"]
        }
    }
}

"use strict";
const e = require("../../common/vendor.js"),
  a = require("../../common/assets.js"),
  t = require("../../api/database.js"),
  n = e.defineComponent({
    __name: "index",
    setup(n) {
      const o = e.ref("");

      function s() {
        e.index.navigateTo({
          url: "/pages/avatar/avatar"
        })
      }
      async function i() {
        let e = await t.db.collection("TaskJiLu").get();
        console.log(e), o.value = JSON.stringify(e)
      }
      return (t, n) => ({
        a: a._imports_0,
        b: e.o(s),
        c: e.o(i),
        d: e.t(o.value)
      })
    }
  });
wx.createPage(n);